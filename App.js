/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import CustomDrawerContent from './src/components/Navigations/CustomDrawerContent';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';


import BeforeLogin from './src/screens/BeforeLogin.js';
import Login from './src/screens/Login.js';
import SignUpWithEmail from './src/screens/SignUpWithEmail.js';
import PreferenceScreen from './src/screens/PreferenceScreen.js';
import UniversalLoadingScreen from './src/screens/UniversalLoadingScreen.js';
// import PreferenceScreen1 from './src/screens/Untitled1';
// import Untitled1 from './src/screens/Untitled1';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { withAuthenticator } from 'aws-amplify-react-native';
// import DrawerNavigator from './src/screens/DrawerNavigator';
import NewsFeed from './src/screens/NewsFeed';
import SampleYoutubeScreen from './src/screens/sampleYoutubeScreen.js';
import SignOutPage from './src/screens/SignOutPage.js';
import TabNavigator from './src/screens/TabNavigator.js';
import Settings from './src/screens/SettingsPage.js';
import PreferenceScreen2 from './src/screens/PreferenceScreen2'
import PrivacyPolicy from './src/screens/PrivacyPolicy.js';
import TermsCond from './src/screens/TermsCond.js';
import LanguagePage1 from './src/screens/LanguagePage1.js';
import ChangeLang from './src/screens/ChangeLang.js';

const MyDrawer = createDrawerNavigator();

const data = require('./src/assets/translations/texts.json');

const App = () => {
  const [isFirstLaunch, setIsFirstLaunch] = useState(false);
  const [appLanguage , setAppLanguage] = useState("English");
  useEffect(()=>{
    async function checkIsFreshInstall()
    {
      AsyncStorage.getItem('alreadyLaunched').then(
        async value=>{
          if(!value)
            setIsFirstLaunch(true);
          else{
            await AsyncStorage.setItem('alreadyLaunched', 'true');
          }
        }
      )
    }
    checkIsFreshInstall();

    
    async function getAppLanguage()
    {
        let appLang = await AsyncStorage.getItem('AppLanguage');
        if(appLang==null)setAppLanguage("English");
        else setAppLanguage(appLang);
        // alert(appLang);
    }
    getAppLanguage();

  },[]);
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerStyle: {
            // backgroundColor: '#32292e',
          },
          // headerTintColor: '#fff',
          headerTitleStyle: {
            // fontWeight: 'bold',
          },
        }}
      >
        {/* <Stack.Screen name="YoutubeScreen" component={SampleYoutubeScreen} /> */}
        {isFirstLaunch
        ?
        <React.Fragment>
        <Stack.Screen name="Choose Language" component={LanguagePage1} />
        <Stack.Screen options={{ headerShown: false }} name="LoadingScreen" component={UniversalLoadingScreen} />
        </React.Fragment>
        :
        <React.Fragment>
        <Stack.Screen options={{ headerShown: false }} name="LoadingScreen" component={UniversalLoadingScreen} />
        <Stack.Screen name="Choose Language" component={LanguagePage1} />
        </React.Fragment>
        }
        
        <Stack.Screen options={{title:data["ChangeAppLanguage"][appLanguage]}} name="Change App Language" component={ChangeLang} />
        <Stack.Screen name="Preferences 2" component={PreferenceScreen2} />
        <Stack.Screen options={{ headerShown: false}} name="TabNavigator" component={TabNavigator}/>
        {/* <Stack.Screen options={{ headerShown: false }} name="DrawerNavigator" component={DrawerNavigator} /> */}
        <Stack.Screen options={{ headerShown: false }} name="NewsFeed" component={NewsFeed} />
        <Stack.Screen options={{ headerShown: false }} name="Login" component={BeforeLogin} />
        <Stack.Screen options={{title:data["PrivacyPolicy"][appLanguage]}} name="Privacy Policy" component={PrivacyPolicy} />
        <Stack.Screen options={{title:data["SelectYourPreferences"][appLanguage]}} name="Select Your Preferences" component={PreferenceScreen} />
        <Stack.Screen options={{title:data["Settings"][appLanguage]}} name="Settings" component={Settings} />
        <Stack.Screen options={{title:data["LoginWith_email"][appLanguage]}}name="Login With Email" component={Login} />
        <Stack.Screen options={{title:data["SignUp"][appLanguage]}} name="Sign Up With Email" component={SignUpWithEmail} />
        <Stack.Screen options={{title:data["TermsAndConditions"][appLanguage]}} name="Terms and Conditions" component={TermsCond} />
        <Stack.Screen options={{ headerShown: false }} name="Sign Out Page" component={SignOutPage} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

const Stack = createStackNavigator();

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: 'blue',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

// export default withAuthenticator(App);
export default App;

