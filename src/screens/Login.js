import React, {Component, useEffect, useState, useRef} from 'react';
import {StyleSheet, ActivityIndicator,  KeyboardAvoidingView, Text, View, Platform} from 'react-native';

import EmailInput from '../components/TextInputs/EmailInput.js';
import PasswordInput from '../components/TextInputs/PasswordInput.js';
import LoginBtn from '../components/OtherBtns/LoginBtn.js';
import Toast from 'react-native-simple-toast';

// import FirstTimeSignIn from '../__api__connections/UserAuth.js';
const UserAuth = require('../__api__connections/UserAuth.js');

import { Auth } from '@aws-amplify/auth';
import Amplify from '@aws-amplify/core';
import awsConfig from '../../src/aws-exports';
import { withAuthenticator } from 'aws-amplify-react-native';
import {CommonActions} from '@react-navigation/native';

import AsyncStorage from '@react-native-community/async-storage';

Amplify.configure(awsConfig);

const data = require('../assets/translations/texts.json');



const Login = (props) =>
{
  const [userNme, setUserNme] = useState("");
  const [Pass, setPass] = useState("");
  const [loginPressed, setLoginPressed] = useState(false);
  const [focusOnEmail, setFocusOnEmail] = useState(false);
  const [focusOnPassword, setFocusOnPassword] = useState(false);
  const [focusOnSubmit, setFocusOnSubmit] = useState(false);
  const [appLanguage, setAppLanguage] = useState(null);

  useEffect(()=>{
    async function getAppLanguage()
    {
        let appLang = await AsyncStorage.getItem('AppLanguage');
        if(appLang==null)setAppLanguage("English");
        else setAppLanguage(appLang);

    }
    getAppLanguage();
  }, []);

  const signIn = async()=>{
    try{
      if(userNme==="") throw("Username Should Not Be Empty!");
      if(Pass==="") throw("Passowrd cannot be empty!");
      if(userNme&&Pass)
      {
        setLoginPressed(true);
        const user = await Auth.signIn(userNme, Pass);
        let userToken = (await Auth.currentSession()).getIdToken();
        // alert(`userToken : ${JSON.stringify(userToken)}`);
        if(typeof user === "object" && user!=null)
        {
          UserAuth.FirstTimeSignIn()
          .then(

            ()=>{
              console.log(`navigating to LoadingScreen`)   ;
              let resetAction = CommonActions.reset({
                index:0,
                routes:[{name:'LoadingScreen'},],
              });
              props.navigation.dispatch(resetAction);
            }
            )
          .catch(e=>{Toast.show(`${JSON.stringify(e)}`, 1);})
        }
      }
    }
    catch(e)
    {
      setLoginPressed(false);
      setFocusOnSubmit(false);
      if(e.code=="UserNotFoundException")
        Toast.show('User Does not Exist!', 1);
      else if(e.code=="NotAuthorizedException")
        Toast.show(`Incorrect Username/Password!`, 1);
      else
        Toast.show(`${JSON.stringify(e)}`, 1)
    }
  };
  const submtBtn = ()=>{
    signIn();
  }
  // useEffect(()=>{
  //   alert(`c`);
  // }, [focusOnPassword])
  return (
    <KeyboardAvoidingView behavior={Platform.OS === "ios"?"padding":null} style={{...styles.mainScreen}}>

      <View style={[styles.infoBlock]}>
        {/* <View style={{...styles.infoTitle}}>
          <Text style={{textAlign:'center', fontSize:25}}>{data["Login"]["English"]}</Text>
        </View> */}
        <View style={[styles.infoText]}>
          <Text style={{width:'80%', marginTop:'5%', marginLeft:'10%', color:'white', fontSize:20}}>{data["SignUpText"][appLanguage]}</Text>
        </View>
      </View>
      <View style={[styles.optionsBlock]}>
        <View style={{flex:0.2}}>
          <EmailInput focusNext={setFocusOnPassword} appLanguage={appLanguage} setEmail={setUserNme} placeHolderText={data["EnterYourEmailID"][appLanguage]}/>
        </View>
        <View style={{flex:0.2}}>
          <PasswordInput focusNext={setFocusOnSubmit} focusOnMe={focusOnPassword} passwordInputRef={a=>passwordRef(a)} language="English" setPassword={setPass} placeHolderText={data["EnterYourPassword"][appLanguage]}/>
        </View>
        {!loginPressed?
        <View style={{flex:0.2}}>
          <LoginBtn focusOnMe={focusOnSubmit} appLanguage={appLanguage} submitBtn={submtBtn}/>
        </View>
        :
        <ActivityIndicator style={{alignSelf:'center', alignItems:'center'}} size="large" color="white" />
        }
      </View>
    </KeyboardAvoidingView>
  )
}

const styles = StyleSheet.create({
  mainScreen:{
    flex:1,
    backgroundColor:'#1F1A1B'
  },
  infoBlock:{
    flex:0.3
  },
  // infoTitle:{
  //   flex:0.2
  // },
  infoText:{
    flex:1
  },
  optionsBlock:{
    flex:0.7
  }
});


export default Login;
// export default withAuthenticator(Login, true);