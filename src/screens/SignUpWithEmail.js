import React, {Component, Fragment, useState, useEffect} from 'react';
import {StyleSheet, ActivityIndicator, Text, View, ToastAndroid, SafeAreaView} from 'react-native';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';

import EmailInput from '../components/TextInputs/EmailInput.js';
import PasswordInput from '../components/TextInputs/PasswordInput.js';
import SignUpBtn from '../components/OtherBtns/SignupBtn.js'
import NameInput from '../components/TextInputs/NameInput.js';

import { Auth } from '@aws-amplify/auth';
import Amplify from '@aws-amplify/core';
import awsConfig from '../../src/aws-exports';
import { withAuthenticator } from 'aws-amplify-react-native';
Amplify.configure(awsConfig);

const data = require('../assets/translations/texts.json');
const SignUpWithEmail = (props) => {
  const [userNme, setUserNme] = useState('');
  const [password, setPass] = useState('');
  const [confirmPass, setConfirmPass] = useState('');
  const [emailID, setEmailID] = useState('');
  const [name, setName] = useState('');
  const [phNo, setPhNo] = useState('');
  const [signUpPressed, setSignUpPressed] = useState(false);
  const [codeSent, setCodeSent] = useState(false);
  const [confirmationCode, setConfirmationCode] = useState('');
  const [focusOnEmail, setFocusOnEmail] = useState(false);
  const [focusOnPassword, setFocusOnPassword] = useState(false);
  const [focusOnConfirmPassword, setFocusOnConfirmPassword] = useState(false);
  const [focusOnSubmit, setFocusOnSubmit] = useState(false);
  const [appLanguage, setAppLanguage] = useState(null);
  useEffect(()=>{
    async function getAppLanguage()
    {
      let appLang = await AsyncStorage.getItem('AppLanguage');
      if(appLang==null)setAppLanguage("English");
      else setAppLanguage(appLang);
    }
    getAppLanguage();
  }, []);
  useEffect(()=>{

    setFocusOnEmail(false);

  },[focusOnPassword]);
  
  useEffect(()=>{

    setFocusOnPassword(false);

  },[focusOnConfirmPassword]);
  
  useEffect(()=>{

    setFocusOnConfirmPassword(false);

  }, [focusOnSubmit]);


  const signUp = ()=>{
    setSignUpPressed(true);

    try {
        // alert(`working!`);
        Auth.signUp({
          username:emailID,
          password:password,
          attributes:{
            email:emailID,
            name:name,
          }
        })
        .then(
          data => {
            setCodeSent(true);
          }
        )
        .catch(
          err=>{
            // throw new Error(err.message);
            

            throw err;
          }
        )
        .catch(
          e=>{
            Toast.show(`${e.message}`);
            setSignUpPressed(false);
            setFocusOnSubmit(false);
            // props.navigation.navigate("LoadingScreen");
          }
        )
    }
    catch (err) {
      console.log('Error signing up : ', error);
      Toast.show(`${JSON.stringify(err)}`)
    }
  };
  return (
    <SafeAreaView style={[styles.mainScreen]}>
      <View style={[styles.infoBlock]}>
        {/* <View style={{...styles.infoTitle}}>
          <Text style={{textAlign:'center', fontSize:25}}>{data["Login"]["English"]}</Text>
        </View> */}
        <View style={[styles.infoText]}>
          <Text style={{width:'80%', marginTop:'5%', marginLeft:'10%', color:'white', fontSize:20}}>{data["SignUpText"][appLanguage]}</Text>
        </View>
      </View>
      {
      codeSent?
      <View style={[styles.optionsBlock]}>
        <View style={{flex:0.2, marginTop:40}}>
          {/* <ConfirmationCode setCode={setConfirmationCode} language="English" placeHolderText={data["ConfirmationCode"]["English"]}/> */}
          <Text style={{color:'#dd4e68', fontWeight:'bold', alignSelf:'center', fontSize:17}}>{data["VerificationText"][appLanguage]}</Text>
        </View>
        {/* <View style={{flex:0.2}}>
          <SignUpBtn language="English"  submitBtn={this.confirmSignUp}/>
        </View> */}
      </View>
      :
      
      <View style={[styles.optionsBlock]}>
        {!signUpPressed?
        <Fragment>
          <View style={{flex:0.2}}>
            <NameInput focusNext={setFocusOnEmail} setName={setName} appLanguage={appLanguage} placeHolderText={data["Name"][appLanguage]}/>
          </View>
          <View style={{flex:0.2}}>
            <EmailInput setFocusOnMe={setFocusOnEmail} focusOnMe={focusOnEmail} focusNext={setFocusOnPassword} setEmail={setEmailID} appLanguage={appLanguage} placeHolderText={data["EnterYourEmailID"][appLanguage]}/>
          </View>
          <View style={{flex:0.2}}>
            <PasswordInput setFocusOnMe={setFocusOnPassword} focusOnMe={focusOnPassword} focusNext={setFocusOnConfirmPassword} setPassword={setPass} language="English" placeHolderText={data["EnterYourPassword"][appLanguage]}/>
          </View>
          <View style={{flex:0.2}}>
            <PasswordInput focusOnMe={focusOnConfirmPassword} focusNext={setFocusOnSubmit} setPassword={setConfirmPass} language="English" placeHolderText={data["ConfirmPassword"][appLanguage]}/>
          </View>
          
          <View style={{flex:0.2}}>
            <SignUpBtn focusOnMe={focusOnSubmit} appLanguage={appLanguage}  submitBtn={signUp}/>
          </View>
        </Fragment>
        :
        <Fragment>
          <ActivityIndicator style={{alignSelf:'center', alignItems:'center'}} size="large" color="white" />
        <Text style={{alignSelf:'center', alignItems:'center', fontSize:20, color:'white', marginTop:20}}>{data["Loading"][appLanguage]}</Text>
        </Fragment>
        }
      </View>
      }
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  mainScreen:{
    flex:1,
    backgroundColor:'#1F1A1B'
  },
  infoBlock:{
    flex:0.3
  },
  infoText: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  optionsBlock:{
    flex:0.7
  }
});

export default SignUpWithEmail;