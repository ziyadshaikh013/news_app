import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, Image, SafeAreaView} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { white1X1 } from 'aws-amplify-react';
import Toast from 'react-native-simple-toast';


import { Auth } from '@aws-amplify/auth';
import Amplify from '@aws-amplify/core';
import awsConfig from '../../src/aws-exports';

// import AsyncStorage from '@react-native-community/async-storage';
import {CommonActions} from '@react-navigation/native';

// const PREFERENCES_ENDPOINT = require('../assets/__API__endpoints__/__backend__apis__urls__.js');
const User_Topics = require('../__api__connections/UserTopics.js');
let data = require('../assets/translations/texts.json');
const UniversalLoadingScreen = (props) =>
{
    const [isUserLoggedIn, setIsUserLoggedIn] = useState(false);
    const [userHasPreferences, setUserHasPreferences] = useState(false);
    const [flatListData, setFlatListData] = useState(false);
    const [appLanguage, setAppLanguage] = useState(null);
    // alert(`${PREFERENCES_ENDPOINT.PREFERENCES()}`);
    const fetchPreferences = async ()=>{
        return User_Topics.GetUserTopics()
        .then(output=>output)
        .catch(e=>{Toast.show(`${JSON.stringify(e)}`)})
    }
    const navigateToScreen = (data, jwtToken)=>{
        let preferencesSelected=false;
        console.log(`data : ${JSON.stringify(data)}`);
        data.forEach(element=>{
            // alert(`${element.isSelected}`)
            if(element.isSelected==true) 
                preferencesSelected=true;
        });
        if(preferencesSelected)
        {
            console.log(`navigating to newFeed`)   ;
            let resetAction = CommonActions.reset({
                index:0,
                routes:[{name:'TabNavigator'},],
            })
            // props.navigation.navigate("DrawerNavigator");
            props.navigation.dispatch(resetAction);
        }
        else
        {
            console.log(`navigating to preferences page!`);
            let resetAction = CommonActions.reset({
                index:0,
                routes:[
                    {name:'Select Your Preferences'},
                    // {params:{authToken:jwtToken}},
                ],
            })
            props.navigation.dispatch(resetAction);
        }
    };
    const checkUserLoggedIn = async ()=>{
        const user = await Auth.currentUserInfo();
        if(typeof user === "object" && user!==null)
        {
            let aa = (await Auth.currentSession()).getIdToken();
            // alert(`jwtToken : ${aa.jwtToken}`);
            fetchPreferences(aa.jwtToken)
            .then(data=>{
              navigateToScreen(data, aa.jwtToken);
            })
            .catch(e=>{
                console.log(`error from fetchPreferences : ${JSON.stringify(e)}`);
                Toast.show(`${JSON.stringify(e)}`);
            })

        }
        else
        {
            let resetAction = CommonActions.reset({
                index:0,
                routes:[{name:'Login'},],
            })
            // props.navigation.navigate("DrawerNavigator");
            props.navigation.dispatch(resetAction);
        }
    };
    useEffect(()=>{
        async function getAppLanguage()
        {
            let appLang = await AsyncStorage.getItem('AppLanguage');
            if(appLang==null) setAppLanguage("English");
            else setAppLanguage(appLang);
            // alert(`appLang : ${appLang}`)

        }
        getAppLanguage();
        checkUserLoggedIn();
    }, []);
    return (
        <SafeAreaView style={[styles.loadingScreen]}>
            <View style={[styles.loadingScreenBlock]}>
                <View style={[styles.loadingAnimation]}>
                    <Image 
                        source={require('../assets/animationGIFS/compass.gif')}
                        style={[styles.animationGif]}
                    />
                </View>
                <View style={[styles.loadingTextBlock]}>
                    <Text style={[styles.loadingText]}>{data["Loading"][appLanguage]}</Text>
                </View>
            </View>
        </SafeAreaView>
    )
}

const styles=StyleSheet.create({
    loadingScreen:{
        flex:1
    },
    loadingScreenBlock:{
        flex:1,
        alignContent:'center',
        alignItems:'center',
    },
    animationGif:{
        // flex:1,
        resizeMode:'cover',
        // width:'50%',
        top:'30%',
        alignSelf:'center',
        alignContent:'center',
        alignItems:'center',
        // height:'50%',
    },
    loadingAnimation:{
        flex:1
    },
    loadingTextBlock:{
        // flex:0.5
        position:'absolute',
        top:'80%',
        left:'42%'
    },
    loadingText:{
        fontSize:20,
        color:'black',
        fontWeight:'bold',
    }
});

export default UniversalLoadingScreen;