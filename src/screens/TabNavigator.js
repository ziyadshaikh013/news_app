import React, {useEffect, useState} from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import NewsFeed from './NewsFeed';
import Search from './Search';
import Bookmark from './Bookmark';
import Profile from './Profile';
import Icons from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';

const Tab = createBottomTabNavigator();

const data = require('../assets/translations/texts.json');


const TabNavigator = (props)=>{
    const [appLanguage, setAppLanguage] = useState("English");
    useEffect(()=>{
        async function getAppLanguage()
        {
            let appLang = await AsyncStorage.getItem('AppLanguage');
            // alert(`appLang : ${appLang}`);
            if(appLang==null)setAppLanguage("English");
            else setAppLanguage(appLang); 
        }
        getAppLanguage();
    },[]);
    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;

                    if (route.name === data["NewsFeed"][appLanguage] ) {
                        iconName = focused ? 'newspaper-o': 'newspaper-o';
                    } else if (route.name === data["Search"][appLanguage]) {
                        iconName = focused ? 'search' : 'search';
                    } else if (route.name === data["Bookmarks"][appLanguage]) {
                        iconName = focused ? 'bookmark' : 'bookmark-o';
                    } else if (route.name === data["Profile"][appLanguage]) {
                        iconName = focused ? 'user-circle' : 'user-circle-o';
                    }

                    return <Icons name={iconName} size={size} color={color} />;
                },
            })}
            // unmountOnBlur={true}
            tabBarOptions={{
                activeTintColor: '#C25C70',
                inactiveTintColor: '#f3f3f3',
                activeBackgroundColor: '#32292E',
                inactiveBackgroundColor: '#372E33',
            }}
            style={
                { backgroundColor: '#C25C70' }
            }
            initialRouteName={data["NewsFeed"][appLanguage]}
            
        >    
            <Tab.Screen  options={{unmountOnBlur:true}} name={data["NewsFeed"][appLanguage]} component={NewsFeed} />
            <Tab.Screen  options={{unmountOnBlur:true}} name={data["Search"][appLanguage]} component={Search} />
            <Tab.Screen  options={{unmountOnBlur:true}} name={data["Bookmarks"][appLanguage]} component={Bookmark} />
            <Tab.Screen  options={{unmountOnBlur:true}} name={data["Profile"][appLanguage]} component={Profile} />
        </Tab.Navigator>
    )
}

export default TabNavigator;
