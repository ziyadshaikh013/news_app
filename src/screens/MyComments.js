/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React from 'react';
import { View, Text, Button } from 'react-native';

function MyComments({ navigation }) {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>My Comments Screen</Text>
            <Button onPress={() => navigation.openDrawer()} title="Open" />
        </View>
    );
}

export default MyComments;