import React, { Component, useState, useEffect } from "react";
import { StyleSheet, FlatList, TouchableOpacity, View, SafeAreaView, ScrollView } from "react-native";
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';
import PropTypes from 'prop-types';
import {CommonActions} from '@react-navigation/native';

import { Auth } from '@aws-amplify/auth';
import Amplify from '@aws-amplify/core';
import awsConfig from '../../src/aws-exports';

import PreferenceCategoryBtn from '../components/OtherBtns/PreferenceCategoryBtn.js';
import PrevNextBtns from '../components/OtherBtns/PrevNextBtns.js';

const User_Topics = require('../__api__connections/UserTopics.js');


function setSelected(objArr)
{
  let temp = new Map();
  objArr.forEach(element=>{
    temp[element.topicId]={selectionId : element.selectionId, isSelected:element.isSelected};
  });
  //console.log(`temp : ${JSON.stringify(temp)}`);
  return temp;
}
function handleSelection(topicId, stateVar, preferencesSelected, preferenceSelectedSetter)
{
  let tmp = stateVar;
  // console.log(`${typeof}`)
  console.log(`tmp : ${JSON.stringify(tmp)}`);
  let tmp2 = preferencesSelected;
  if(tmp2.hasOwnProperty(topicId))
  {
    delete tmp2[topicId];
  }
  else
  {
    tmp2[topicId] = tmp[topicId];
    tmp2[topicId].isSelected = tmp[topicId].isSelected===true?false:true;
    // tmp2[topicId].selectionId = tmp[topicId].selectionId;
  }
  //console.log(`tmp2 : ${JSON.stringify(tmp2)}`);
  preferenceSelectedSetter(tmp2);
}
const PreferenceScreen = (props) => {
  const [flatListData, setFlatListData] = useState([]);
  const [requestBodyObj, setRequestBodyObj] = useState({});
  const [selectedPreferences, setSelectedPreferences] = useState(new Map());
  const [appLanguage, setAppLanguage] = useState(null);

  const selectionOfID = (objId)=>{
    // console.log(`inside selectionofID()`);
    console.log(`flatListData : ${JSON.stringify(flatListData)}`);
    handleSelection(objId, requestBodyObj, selectedPreferences, setSelectedPreferences);
  };
  useEffect(()=>{
    async function getAppLanguage()
    {
        let appLang = await AsyncStorage.getItem('AppLanguage');
        setAppLanguage(appLang);
        if(appLang==null)setAppLanguage("English");
    }
    getAppLanguage();
    if(!flatListData.length)
    {
      const fetchPreferences = async ()=>{
        User_Topics.GetUserTopics()
        .then(output=>{
          setFlatListData(output);
          let tmpp = setSelected(output);
          console.log(`tmpp : ${JSON.stringify(tmpp)}`);
          setRequestBodyObj(tmpp);
        })
        .catch(e=>{Toast.show(`${JSON.stringify(e)}`)})
       };
       fetchPreferences();
    }
  }, []);
  const submitPreferences = async ()=>{
    let selObjDta = [];
    console.log(`selectedPreferences : ${JSON.stringify(selectedPreferences)}`)
    for(let keys in selectedPreferences)
    {
      // console.log(`selectedPreferences[keys] : ${JSON.stringify(selectedPreferences[keys].isSelected)}`)
      let bool = selectedPreferences[keys].isSelected;
      selObjDta.push({"topicId":parseInt(keys), "isSelected":bool});
    }
    let aa = (await Auth.currentSession()).getIdToken();
    console.log(`${JSON.stringify(selObjDta)}`);
    User_Topics.SubmitUserTopics(selObjDta)
    .then(
      respData=>{
        // alert(`${JSON.stringify(respData)}`);
        if(respData.message==="Topics Saved Successfully")
        {
          props.navigation.navigate('Preferences 2');
        }
        else if(respData.message === "At least two topics must be selected")
        {
          Toast.show(`Please Select atleast 2 Topics!`, 1)
        }
      }
    )
    .catch(e=>{
      Toast.show(`${JSON.stringify(e)}`)
    })
  };
  return (
    <SafeAreaView style={styles.container}>
      {/* <ScrollView style={styles.categoryList}> */}
        <FlatList
          data={flatListData}
          renderItem={({item})=>{
            return (<PreferenceCategoryBtn HandleSelection={selectionOfID} displayData={item}/>)
          }}
          numColumns={3}
          keyExtractor={(item, index) => index.toString()}
        />
      {/* </ScrollView> */}
      <View style={styles.navigationBtns}>
        {/* <PrevNextBtns totalPages={1} appLanguage={appLanguage} NextCallback={submitPreferences} /> */}
        <PrevNextBtns activeNext={true} activatePrevious={false} appLanguage={appLanguage} NextCallback={submitPreferences} PreviousCallBack={function(){}} />
      </View>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  categoryList: {
    flex: 1,
    padding: 10,
  },
  navigationBtns:{
    height: 70,
    justifyContent: 'center',
    alignItems: 'center'
  },
});
export default PreferenceScreen;
