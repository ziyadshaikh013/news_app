import React, { Component, useEffect, useState } from 'react';
import {
  Text,
  StyleSheet,
  Image,
  View,
  TouchableOpacity
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';


// import SignUpBtn from '../components/SignUpBtn.js';
import SignUpBtn from '../components/SignUpLink.js';
import ContinueWithFacebook from '../components/SocialAccounts_SignInBtns/ContinueWithFacebook.js';
import ContinueWithGoogle from '../components/SocialAccounts_SignInBtns/ContinueWithGoogle.js';
import ContinueWithEmail from '../components/SocialAccounts_SignInBtns/ContinueWithEmail.js';
import { withSafeAreaInsets } from 'react-native-safe-area-context';

import { Auth, Hub } from '@aws-amplify/auth';
import Amplify from '@aws-amplify/core';
import awsConfig from '../../src/aws-exports';
import { withOAuth } from 'aws-amplify-react';

const data = require('../assets/translations/texts.json');

const BeforeLogin = (props) =>
{
  const [appLanguage, setAppLanguage] = useState(null);
  useEffect(()=>{
    async function getAppLanguage()
    {
        let appLang = await AsyncStorage.getItem('AppLanguage');
        if(appLang==null)setAppLanguage("English");
        else setAppLanguage(appLang);
        // alert(appLang);
    }
    getAppLanguage();
  }, []);

  const signInToGoogle = ()=>{
    Auth.federatedSignIn({provider: 'Google'});
  }
  return (
    <View style={[styles.mainScreen]}>
      <View style={{flex:0.6}}>
        <TouchableOpacity onPress={()=>props.navigation.navigate("Choose Language")}>
          <Icon name="gear" size={24} color='white' style={styles.iconStyle}></Icon>
        </TouchableOpacity>
        <View style={{flex:0.8}}>
          <Image 
            style={[styles.mainImage]}
            source={require('../assets/images/appLogo.png')}
          />
        </View>
        {/* <View style={{flex:0.2}}>
          <Text style={[styles.title]}>{data["Updates"]["English"]}</Text>
        </View> */}
        
      </View>
      <View style={{flex:0.4}}>
        <View style={{flex:0.3}}>
          <ContinueWithGoogle signInCallback={signInToGoogle} appLanguage={appLanguage} />
        </View>
        <View style={{flex: 0.3}}>
          <ContinueWithFacebook appLanguage={appLanguage} />
        </View>
        <View style={{flex:0.3}}>
          <ContinueWithEmail navigateTo = {props.navigation} appLanguage={appLanguage}/>
        </View>
        <View style={{flex:0.2}}>
          <SignUpBtn navigateTo = {props.navigation} appLanguage={appLanguage}  />
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  mainScreen:{
    backgroundColor:'#1F1A1B',
    flex:1
  },
  title:{
    color:'white',
    alignSelf:'center',
    alignItems:'center',
    fontSize:25,
    fontWeight:'bold',
  },
  mainImage:{
    height:400,
    width:400
  },
  iconStyle: {
    alignContent: 'center',
    alignSelf: 'flex-end',
    marginTop:20,
    marginRight:20,
    // position:'absolute'
  },
})
export default BeforeLogin;