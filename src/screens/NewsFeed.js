import React, {useState, useEffect} from 'react';
import {StyleSheet, View, SafeAreaView, Text} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import TopNavigation from '../components/Navigations/TopNavigation.js';
import LatestNewsFlatList from '../components/Content/LatestNewsFlatList.js';
import NewsFeedContent from '../components/Content/NewsFeedContent.js';

const data = require('../assets/translations/texts.json');

// import 
const NewsFeed = (props) => {
    const [appLanguage, setAppLanguage] = useState("English");
    const [intentionalOverLay, setIntentionalOverLay] = useState(false);
    // useEffect(()=>{
    //     alert(`intentionalOverlay : ${intentionalOverLay}`);
    // },[intentionalOverLay])
    useEffect(()=>{
        async function getAppLanguage()
        {
            let appLang = await AsyncStorage.getItem('AppLanguage');
            if(appLang==null)setAppLanguage("English");
            else setAppLanguage(appLang);
            // alert(appLang);
        }
        getAppLanguage();
    }, []);
    return (
        <SafeAreaView style={[styles.feedScreen]}>
                {/* Top bar */}
                <View style={[styles.topNav]}>
                    <TopNavigation setOverlay={setIntentionalOverLay} appLanguage={appLanguage} navigation={props.navigation} />
                </View>
                
                {/* Middle Content */}
                <View style={[styles.Content]}>
                    <NewsFeedContent intentionalOverLay={intentionalOverLay} />
                </View>
                
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    feedScreen: {
        flex: 1,
        backgroundColor:'#dbd6d6',
    },
    topNav: {
        flex: 0.1
    },
    Content: {
        flex: 0.9
    },
})
// 912d2d
// 939393
export default NewsFeed;