import React, {useState, useEffect, useRef} from 'react';
import { View, Text, StyleSheet, SafeAreaView, ScrollView, ToastAndroid } from 'react-native';
import PrevNextBtns from '../components/OtherBtns/PrevNextBtns';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import Icons from 'react-native-vector-icons/Entypo';
import {CommonActions} from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';

import Toast from 'react-native-simple-toast';

const StatesAPI = require('../__api__connections/States.js');
const TopicsAPI = require('../__api__connections/UserTopics.js');
const data = require('../assets/translations/texts.json');

const PreferenceScreen2 = props =>{
  const [states, setStates] = useState({selectedItems: []});
  const [city, setCity] = useState({selectedItems: []})
  const [stateList, setStateList] = useState({name:"India", "id":0, "children":[]});
  const [stateListMap, setStateListMap] = useState({});
  const [cityList, setCityList] = useState({name:"Recommended Cities", "id":0, "children":[]});
  const [cityListMap, setCityListMap] = useState({});
  const [statesAreSelected, setStatesAreSelected] = useState(false);
  const [appLanguage, setAppLanguage] = useState(null);

  const getStates = () => {
    StatesAPI.GetListedStates()
    .then(
      data=> {
        let customObj = [{name:"India", 'id':0, "children":data}];
        let tempMap = new Map();
        for(let i in data)
          tempMap.set(data[i].id, data[i].topicId);
        setStateList(customObj);
        setStateListMap(tempMap);
      })
    .catch(
      e=>{Toast.show(`Error Occured!`, 1)}
    );
    }
  const getCities = state_id => {
    StatesAPI.GetStateCities(state_id).then(
      data=>{
        // alert(`data : ${JSON.stringify(data)}`);
        let customObj = [{name:"Recommended Cities", 'id':0, "children":data}];
        let tempMap = new Map();
        for(let i in data)
          tempMap.set(data[i].id, data[i].topicId);
        setCityList(customObj);
        // console.log(`tempMap : ${tempMap}`);
        setStatesAreSelected(false);
        setCityListMap(tempMap);
      }
    )
    .catch(
      e=>{Toast.show(`Error Occured!`, 1)}
    );
  }
  useEffect(() => {
    getStates();
    async function getAppLanguage()
    {
        let appLang = await AsyncStorage.getItem('AppLanguage');
        setAppLanguage(appLang);
        if(appLang==null)setAppLanguage("English");
        // alert(appLang);
    }
    getAppLanguage();
  },[]);
  const onSelectedStateChange = (selectedItems) => {
    // alert(`selectedItems : ${selectedItems}`);
      setStates({...states, selectedItems});
      console.log(`selectedItems : ${selectedItems}`);
  }
  const onSelectedCityChange = (selectedItems) => {
      setCity({...city, selectedItems});
      console.log(selectedItems);
  }
  const handleStateConfirm = () => {
    setStatesAreSelected(true);
  }
  useEffect(()=>{
    if(statesAreSelected) getCities(states.selectedItems[0]);
  },[statesAreSelected])
  const handleCityConfirm = () => {
  }
  const saveTopics = ()=>{
    let selectedObjects = [];
    for(let i of states.selectedItems)
    {
      selectedObjects.push({"topicId":stateListMap.get(i), "isSelected":true})
    }
    for(let i of city.selectedItems)
    {
      selectedObjects.push({"topicId":cityListMap.get(i), "isSelected":true})
    }
    // alert(`selectedObjects : ${JSON.stringify(selectedObjects)}`);
    TopicsAPI.SubmitUserTopics(selectedObjects)
    .then(
      (data)=>{
        if(data.message === "Topics Saved Successfully")
        {
          let resetAction = CommonActions.reset({
            index:0,
            routes:[{name:'TabNavigator'},],
          });
          props.navigation.dispatch(resetAction);
        }
      }
    )
  }
  return (
    <SafeAreaView style={{flex: 1}}>
            <View style={styles.wrapper}>
              <ScrollView>
                    <View style={styles.content}>
                            <Icons name="location" size={60} style={{paddingBottom: 20}} color="#D55164"/>
                            <Text style={styles.text}>{data["GetLocationTitle"][appLanguage]}</Text>
                    </View>
                    <View>
                        <SectionedMultiSelect
                            styles={fieldStyle}
                            items={stateList}
                            uniqueKey="id"
                            subKey="children"
                            selectText={data["ChooseStateText"][appLanguage]}
                            showDropDowns={false}
                            readOnlyHeadings={true}
                            onSelectedItemsChange={onSelectedStateChange}
                            selectedItems={states.selectedItems}
                            onConfirm={handleStateConfirm}
                            selectedIconComponent={<Icons name="check" color="green" />}
                        />
                    </View>
                    <View>
                        <SectionedMultiSelect
                            styles={fieldStyle}
                            items={cityList}
                            uniqueKey="id"
                            subKey="children"
                            selectText={data["ChooseCityText"][appLanguage]}
                            showDropDowns={false}
                            readOnlyHeadings={true}
                            onSelectedItemsChange={onSelectedCityChange}
                            selectedItems={city.selectedItems}
                            onConfirm={handleCityConfirm}
                        />
                    </View>
                  </ScrollView>
                </View>
            <View style={styles.navigationBtns}>
            <PrevNextBtns activeNext={true} activatePrevious={true} PreviousCallBack={function(){}} totalPages={1} NextCallback={saveTopics} appLanguage={appLanguage}/>
        </View>
    </SafeAreaView>
  )
}
const fieldStyle ={
    selectToggle: {
        width: '90%',
        borderWidth: 1,
        borderColor: 'black',
        padding: 10,
        borderRadius: 20,
        alignSelf: 'center',
        marginVertical: 20
    },
    selectToggleText: {
        color: 'grey'
    },
    chipsWrapper: {
      padding: 10
    }
}
const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    navigationBtns: {
        height: 70,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    content: {
        height: 200,
        width: "90%",
        backgroundColor: 'white',
        borderRadius: 20,
        paddingHorizontal: 30,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20
    },
    text:{
        fontSize: 18,
        textAlign: 'center',
        color: 'black',
        fontWeight: 'bold'
    }
});
export default PreferenceScreen2;