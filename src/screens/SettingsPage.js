import React, {useEffect, useState} from 'react'
import { View, Text, SafeAreaView, ScrollView, StyleSheet, Dimensions} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';

const data = require('../assets/translations/texts.json');

import { Auth } from '@aws-amplify/auth';
import Amplify from '@aws-amplify/core';
import awsConfig from '../aws-exports';
import { TouchableOpacity } from 'react-native-gesture-handler';


function Settings(props){
  const [nameOfUser, setNameOfUser] = useState('');
  const [emailOfUser, setEmailOfUser] = useState('');
  const [appLanguage, setAppLanguage] = useState(null);
  useEffect(()=>{
    async function getAppLanguage()
    {
        let appLang = await AsyncStorage.getItem('AppLanguage');
        setAppLanguage(appLang);
        if(appLang==null)setAppLanguage("English");
        // alert(appLang);
    }
    getAppLanguage();
    if(!nameOfUser&&!emailOfUser)
    {
      Auth.currentUserInfo().
      then(
          resp=>
          {
              setNameOfUser(resp.attributes.name);
              setEmailOfUser(resp.attributes.email);
          }
      );
    }
  },[]);
  return (
    <SafeAreaView style={[styles.profilePageView]}>
      <ScrollView style={[styles.profPgScrlView]}>
          <View style={styles.userDet}>
            <View style={[styles.userAvatar]}>
              <Icon name="user-circle" size={56} color="grey" />
            </View>
            <View style={[styles.userDetailsTxt]}>
                <Text
                    style={[styles.text, {
                        fontWeight: 'bold',
                        textTransform: 'capitalize',
                        alignSelf:'center',
                        fontSize: 20,
                    }]}>
                    {nameOfUser}
                </Text>
                <Text
                    style={[styles.text, {
                        // color: '#CAC6C6',
                    }]}>{emailOfUser}</Text>
            </View>
        </View>
        <View style={[styles.options]}>
          <View style={[styles.optnBlck, {borderTopWidth:0.5, borderTopColor:'grey',}]}>
            <TouchableOpacity style={{flexDirection:'row', flex:1}} onPress={()=>props.navigation.navigate("Change App Language")}>
              <View style={[styles.optionsTxtBlck]}>
                  <Text style={[styles.optionsTxt]}>{data["ChangeAppLanguage"][appLanguage]}</Text>
              </View>
              <View style={[styles.optionsArrowBlck]}>
                <Icon style={[styles.optionsArrow]} name="angle-right" size={20} color="grey" />
              </View>
            </TouchableOpacity>
          </View>
          <View style={[styles.optnBlck]}>
            <TouchableOpacity style={{flexDirection:'row', flex:1}}>
              <View style={[styles.optionsTxtBlck]}>
                <Text style={[styles.optionsTxt]}>{data["VideoPlayerSettings"][appLanguage]}</Text>
              </View>
              <View style={[styles.optionsArrowBlck]}>
                <Icon style={[styles.optionsArrow]} name="angle-right" size={20} color="grey" />
              </View>
            </TouchableOpacity>
          </View>
          <View style={[styles.optnBlck]}>
            <TouchableOpacity style={{flexDirection:'row', flex:1}} onPress={()=>props.navigation.navigate("Select Your Preferences")}>
              <View style={[styles.optionsTxtBlck]}>
                <Text style={[styles.optionsTxt]}>{data["EditPrefferedNewsTopics"][appLanguage]}</Text>
              </View>
              <View style={[styles.optionsArrowBlck]}>
                <Icon style={[styles.optionsArrow]} name="angle-right" size={20} color="grey" />
              </View>
            </TouchableOpacity>
          </View>
          <View style={[styles.optnBlck]}>
            <TouchableOpacity style={{flexDirection:'row', flex:1}} onPress={()=>props.navigation.navigate("Privacy Policy")}>
              <View style={[styles.optionsTxtBlck]}>
                <Text style={[styles.optionsTxt]}>{data["PrivacyPolicy"][appLanguage]}</Text>
              </View>
              <View style={[styles.optionsArrowBlck]}>
                <Icon style={[styles.optionsArrow]} name="angle-right" size={20} color="grey" />
              </View>
            </TouchableOpacity>
          </View>
          <View style={[styles.optnBlck]}>
            <TouchableOpacity style={{flexDirection:'row', flex:1}} onPress={()=>props.navigation.navigate("Terms and Conditions")}>
              <View style={[styles.optionsTxtBlck]}>
                <Text style={[styles.optionsTxt]}>{data["TermsAndConditions"][appLanguage]}</Text>
              </View>
              <View style={[styles.optionsArrowBlck]}>
                <Icon style={[styles.optionsArrow]} name="angle-right" size={20} color="grey" />
              </View>
            </TouchableOpacity>
          </View>
          <View style={[styles.optnBlck]}>
            <TouchableOpacity style={{flexDirection:'row', flex:1}} onPress={()=>props.navigation.navigate("Sign Out Page")}>
              <View style={[styles.optionsTxtBlck]}>
                <Text style={[styles.optionsTxt]}>{data["Logout"][appLanguage]}</Text>
              </View>
              <View style={[styles.optionsArrowBlck]}>
                <Icon style={[styles.optionsArrow]} name="angle-right" size={20} color="grey" />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
    )
}

const styles = StyleSheet.create({
  userAvatar:{
    // flex:0.5,

    alignSelf:'center',
    // backgroundColor:'red',
  },
  options:{
    // backgroundColor:'green',
  },
  userDetailsTxt:{
    flex:0.5,
    
  },
  userDet: {
      // flex: 0.2,
      // flex:1,
      marginTop:20,
      height:120,
      // flexDirection: 'row',
      // backgroundColor:'blue',
      
      alignItems: 'center',
  },
  optionsTxtBlck:{
    fontSize:15,
    flex:0.8,
    // backgroundColor:'orange',
  },
  optionsTxt:{
    fontSize:15,
  },
  optionsArrowBlck:{
    flex:0.2,
    alignSelf:'flex-end',
    alignItems:'flex-end',
    // backgroundColor:'yellow',
  },
  optnBlck:{
    flex:0.2,
    padding:20,
    borderBottomWidth:0.5,
    borderBottomColor:'grey',
  },
  backbtn: {
      paddingHorizontal: 10,
      paddingTop: 10,
  },
  profile: {
      height: 70,
      width: 70,
      marginHorizontal: 15,
      borderRadius: 50,
      borderWidth: 2,
      borderColor: 'white',
      alignSelf: 'center',
      justifyContent: 'center',
  },
  text: {
      // color: '',
      fontFamily: 'open san',
  },
  logout: {
      fontSize: 16,
      fontWeight: 'bold',
      color: '#C25C70',
  }
});

export default Settings
