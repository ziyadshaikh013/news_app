/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, {useState, useEffect} from 'react';
import { View, Text, Button, TouchableOpacity, StyleSheet } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {CommonActions} from '@react-navigation/native';

const data = require('../assets/translations/texts.json');

const ChangeLang = (props)=>{

    const [appLanguage, setAppLanguage] = useState(null);
    const [appCurrentLanguage, setAppCurrentLanguage] = useState("English");
    const [contentLanguage, setContentLanguage] = useState({});
    const [choosenContentLanguages, setChoosenContentLanguages] = useState(false);
    useEffect(()=>{
        async function getAppLanguage()
        {
            let appLang = await AsyncStorage.getItem('AppLanguage');
            if(appLang==null) setAppCurrentLanguage("English");
            else setAppCurrentLanguage(appLang);
            // alert(`appLang : ${appLang}`)

        }
        getAppLanguage();
    }, []);
    useEffect(()=>{
        async function setLang()
        {
            if(appLanguage)
            {
                await AsyncStorage.setItem('AppLanguage', appLanguage);
                let resetAction = CommonActions.reset({
                    index:0,
                    routes:[
                        {name:'LoadingScreen'},
                    ],
                })
                if(appLanguage!=appCurrentLanguage)
                props.navigation.dispatch(resetAction);
            }
            else
                await AsyncStorage.setItem('AppLanguage', '');
            
        }
        setLang();
        
    }, [appLanguage]);

    return (
        <View style={{ flex: 1 }}>
            <View style={{flex:0.1}}></View>
            <Text>{data["ChooseAppLanguage"][appCurrentLanguage]}</Text>
            <View style={{flex:0.9}}>
                <View style={[styles.languageBox]}>
                    <TouchableOpacity style={[styles.languageBtn]} onPress={()=>setAppLanguage("English")}>
                        <Text>{data["English"]}</Text>
                    </TouchableOpacity>
                </View>
                <View style={[styles.languageBox]}>
                    <TouchableOpacity style={[styles.languageBtn]} onPress={()=>setAppLanguage("Hindi")}>
                        <Text>{data["Hindi"]}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    languageBox:{
       flex:0.2
    },
    languageBtn:{
        backgroundColor:'blue',
        // borderRadius:20,
        margin:5,
        flex:1,
    }
})

export default ChangeLang;