import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, FlatList, Dimensions, SafeAreaView, ScrollView, Image, ToastAndroid } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

const SCREEN_WIDTH = Dimensions.get('window').width;

import GetBookmarks from '../__api__connections/Bookmarks.js';
import NewsBlock from '../components/Content/NewsBlock.js';
import TopNavigation from '../components/Navigations/TopNavigation.js';

async function LoadMoreData(flatListData, flatListSetter, latestNewsPageNo, setLatestNewsPageNo, itemsPerPage)
{
    let temp = flatListData;
    let fetchedData = await GetBookmarks.GetBookmarks(latestNewsPageNo, itemsPerPage);
    if(fetchedData.length)
    {
        temp.push(...fetchedData);
        flatListSetter(temp);
        setLatestNewsPageNo(latestNewsPageNo+1);
    }
        
}
const Bookmark = props=>
{
    const [latestNewsData, setLatestNewsData] = useState([]);
    const [loadedInitially, setLoadedInitially] = useState(false);
    const [latestNewsPageNo, setLatestNewsPageNo] = useState(1);
    const [itemsPerPage, setItemsPerPage] = useState(10);
    const [markContentEnd, setMarkContentEnd] = useState(false);
    const [contentIsLoading, setContentIsLoading] = useState(true);
    const [loadingBlockInserted, setLoadingBlockInserted] = useState(false);
    const [allDataFetched, setAllDataFetched] = useState(false);
    const [currentVisibleVideo, setCurrentVisibleVideo] = useState(0);
    const [autoStartVideo, setAutoStartVideo] = useState(true);

    const [appLanguage, setAppLanguage] = useState("English");
    useEffect(()=>{
        async function getAppLanguage()
        {
            let appLang = await AsyncStorage.getItem('AppLanguage');
            if(appLang==null)setAppLanguage("English");
            else setAppLanguage(appLang);
            // alert(appLang);
        }
        getAppLanguage();
    }, []);

    const renderFooter = ()=>{
        return (
            <View style={[styles.loadingBlock]}>
                <Image
                    source={require('../assets/animationGIFS/compass.gif')}
                    style={[styles.loadingIcon]}
                />
                {/* <Text>Loading...</Text> */}
            </View>
        )
    };
    const onViewRef = React.useRef((viewableItems)=> {
        setCurrentVisibleVideo(viewableItems.viewableItems[0].index);
    })
    const viewConfigRef = React.useRef({ viewAreaCoveragePercentThreshold: 50 })
  
    const onViewableItemChanged = ({viewableItem, changed})=>{
        console.log(`YOYO : ${viewableItem[0].index}`);
    };
    let flatListRef=null;
    useEffect(()=>{
        if(!loadedInitially)
        {
            LoadMoreData(latestNewsData, setLatestNewsData, latestNewsPageNo, setLatestNewsPageNo, itemsPerPage);
            setLoadedInitially(true);
        }
        // fetchData();
    },[loadedInitially]);
    const _goToNextPage = () => {
        if (currentVisibleVideo >= latestNewsData.length-1) currentVisibleVideo = 0;
        flatListRef.scrollToIndex({
        index: setCurrentVisibleVideo(currentVisibleVideo+1),
        animated: true,
       });
     };
    return (
        <SafeAreaView style={[styles.newsFeed]}>
            <View style={[styles.topNav]}>
                    <TopNavigation appLanguage={appLanguage} navigation={props.navigation} />
            </View>
            <View style={{flex:0.9}}>
                <FlatList
                    // horizontal={true}
                    ref={input=>flatListRef=input}
                    style = {[styles.flatList]} 
                    data={latestNewsData}
                    onViewableItemsChanged={onViewRef.current}
                    viewabilityConfig={viewConfigRef.current}
                    onReachedThreshold={0.8}
                    onEndReached={()=>LoadMoreData(latestNewsData, setLatestNewsData, latestNewsPageNo, setLatestNewsPageNo, itemsPerPage)}
                    // ListFooterComponent={renderFooter}
                    renderItem={({item, index})=>{
                        return(
                            <NewsBlock 
                                autoStartVideo={autoStartVideo}
                                autoStartVideoSetter={setAutoStartVideo}
                                displayData={item}
                                myIndex={index}
                                isRowActive={true}
                                currentIndex={currentVisibleVideo}
                                scrollToNext = {_goToNextPage}
                            />
                        )
                    }}
                    keyExtractor={(item, index) => index.toString()} 
                />

            </View>
        </SafeAreaView>
    );
};

const styles=StyleSheet.create({
  newsFeed:{
      flex:1,
      // flexDirection:'row',
  },
  flatList:{
      flex:1
  },
  topNav: {
      flex: 0.1
  },
  loadingBlock:{
      // height:'90%',
      flex:1,
      // marginTop:'5%',
      width:(SCREEN_WIDTH*0.75),
      // borderRadius:20,    
      alignItems:'center',
      // backgroundColor:'yellow',
  },
  loadingIcon:{
      flex:0.99,
      width:'40%',
      alignItems:'center',
      // height:80,
      // backgroundColor:'red',
      resizeMode:'contain',
  },
});

export default Bookmark;
