import React, {useEffect, useState} from 'react'
import { View, Text, SafeAreaView, ScrollView, StyleSheet, Dimensions} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';


import { Auth } from '@aws-amplify/auth';
import Amplify from '@aws-amplify/core';
import awsConfig from '../aws-exports';
import { TouchableOpacity } from 'react-native-gesture-handler';


function Profile(props){
  const [nameOfUser, setNameOfUser] = useState('');
  const [emailOfUser, setEmailOfUser] = useState('');
  useEffect(()=>{
    if(!nameOfUser&&!emailOfUser)
    {
        Auth.currentUserInfo().
        then(
            resp=>
            {
                setNameOfUser(resp.attributes.name);
                setEmailOfUser(resp.attributes.email);
            }
        );
    }
  });
  return (
    <SafeAreaView style={[styles.profilePageView]}>
      <Text>Page Under Construction</Text>
    </SafeAreaView>
    )
}

const styles = StyleSheet.create({
  userAvatar:{
    // flex:0.5,

    alignSelf:'center',
    // backgroundColor:'red',
  },
  options:{
    // backgroundColor:'green',
  },
  userDetailsTxt:{
    flex:0.5,
    
  },
  userDet: {
      // flex: 0.2,
      // flex:1,
      marginTop:20,
      height:120,
      // flexDirection: 'row',
      // backgroundColor:'blue',
      
      alignItems: 'center',
  },
  optionsTxtBlck:{
    fontSize:15,
    flex:0.5,
    // backgroundColor:'orange',
  },
  optionsTxt:{
    fontSize:15,
  },
  optionsArrowBlck:{
    flex:0.5,
    alignSelf:'flex-end',
    alignItems:'flex-end',
    // backgroundColor:'yellow',
  },
  optnBlck:{
    flex:0.2,
    padding:20,
    borderBottomWidth:0.5,
    borderBottomColor:'grey',
  },
  backbtn: {
      paddingHorizontal: 10,
      paddingTop: 10,
  },
  profile: {
      height: 70,
      width: 70,
      marginHorizontal: 15,
      borderRadius: 50,
      borderWidth: 2,
      borderColor: 'white',
      alignSelf: 'center',
      justifyContent: 'center',
  },
  text: {
      // color: '',
      fontFamily: 'open san',
  },
  logout: {
      fontSize: 16,
      fontWeight: 'bold',
      color: '#C25C70',
  }
});

export default Profile
