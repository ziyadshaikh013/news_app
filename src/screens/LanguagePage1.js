import React, {useState, useEffect} from 'react';
import {View, StyleSheet, Text, ScrollView, TouchableOpacity} from 'react-native';
import {CommonActions} from '@react-navigation/native';

import AsyncStorage from '@react-native-community/async-storage';

import PrevNextBtns from '../components/OtherBtns/PrevNextBtns.js';
const data = require('../assets/translations/texts.json');

const LanguagePage1 = props =>{

    const [appLanguage, setAppLanguage] = useState(null);
    const [contentLanguage, setContentLanguage] = useState({});
    const [choosenContentLanguages, setChoosenContentLanguages] = useState(false);
    
    useEffect(()=>{
        async function setLang()
        {
            if(appLanguage)
                await AsyncStorage.setItem('AppLanguage', appLanguage);
            else
                await AsyncStorage.setItem('AppLanguage', '');
            
        }
        setLang();
    }, [appLanguage]);

    const addAppContentLanguage = (langName)=>{
        let k = choosenContentLanguages;
        if(k.has(langName))
            k.delete(langName);
        else
            k.set(langName, true);
        setChoosenContentLanguages(k);
    }
    const nextBtn = ()=>{
        let resetAction = CommonActions.reset({
            index:0,
            routes:[
                {name:'LoadingScreen'},
            ],
        })
        props.navigation.dispatch(resetAction);
    }

    const prevBtn = ()=>{
        setAppLanguage(null);
    }

    return (
        <ScrollView style={{flex:1}}>
            {appLanguage==null?
            <React.Fragment>
            <Text>Choose App Language</Text>
            <View style={[styles.languageBox]}>
                <TouchableOpacity style={[styles.languageBtn]} onPress={()=>setAppLanguage("English")}>
                    <Text>{data["English"]}</Text>
                </TouchableOpacity>
            </View>
            <View style={[styles.languageBox]}>
                <TouchableOpacity style={[styles.languageBtn]} onPress={()=>setAppLanguage("Hindi")}>
                    <Text>{data["Hindi"]}</Text>
                </TouchableOpacity>
            </View>
            </React.Fragment>
            :
            <React.Fragment>
            <Text>{data["Choose Content Language"][appLanguage]}</Text>
            <View style={[styles.languageBox]}>
                <TouchableOpacity style={[styles.languageBtn]} onPress={()=>addAppContentLanguage["English"]}>
                    <Text>{data["English"]}</Text>
                </TouchableOpacity>
            </View>
            <View style={[styles.languageBox]}>
                <TouchableOpacity style={[styles.languageBtn]} onPress={()=>addAppContentLanguage["Hindi"]}>
                    <Text>{data["Hindi"]}</Text>
                </TouchableOpacity>
            </View>
            <PrevNextBtns activeNext={true} activatePrevious={true} appLanguage={appLanguage} NextCallback={nextBtn} PreviousCallBack={prevBtn} />
            
            </React.Fragment>
            }
        </ScrollView>
    )   
};


const styles = StyleSheet.create({
    languageBox:{
        height:100,
        width:100,
       
    },
    languageBtn:{
        backgroundColor:'blue',
        // borderRadius:20,
        margin:5,
        flex:1,
    }
});

export default LanguagePage1;