import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, TextInput, FlatList, Dimensions, SafeAreaView, ScrollView, Image, ToastAndroid } from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";
import AsyncStorage from '@react-native-community/async-storage';

const SCREEN_WIDTH = Dimensions.get('window').width;

import GetBookmarks from '../__api__connections/Bookmarks.js';
import NewsBlock from '../components/Content/NewsBlock.js';
import TopNavigation from '../components/Navigations/TopNavigation.js';
import { TouchableOpacity } from 'react-native-gesture-handler';
import SearchInput from '../components/TextInputs/SearchInput.js';
import VideosFlatList from '../components/Content/VideosFlatList.js';
const SearchAPI = require('../__api__connections/SearchVideos.js');

const handleSearch = (searchKey, pageNo, noOfItems)=>{
  return SearchAPI.SearchForVideos(noOfItems, pageNo, searchKey)
  .then(
    (resp)=>{
      return resp;
    }
  )
}
const Search = (props)=>{
    const [searchKey, setSearchKey] = useState('');
    const [focusOnSearch, setFocusOnSearch] = useState(false);
    const [flatListData, setFlatListData] = useState(false);
    const [appLanguage, setAppLanguage] = useState("English");
    const [requestForData, setRequestForData] = useState(false);
    const [clearPrevData, setClearPrevData] = useState(false);

    useEffect(()=>{
        async function getAppLanguage()
        {
            let appLang = await AsyncStorage.getItem('AppLanguage');
            if(appLang==null)setAppLanguage("English");
            else setAppLanguage(appLang);
            // alert(appLang);
        }
        getAppLanguage();
    }, []);

    const LoadMoreData = async(flatListData, latestNewsPageNo, itemsPerPage, loadingMoreData)=>
    {
      let temp=[];
      if(loadingMoreData)
        temp = flatListData;
      
        // alert(`${searchKey}, ${latestNewsPageNo}, ${itemsPerPage}`)
        let fetchedData = await handleSearch(searchKey, latestNewsPageNo, itemsPerPage);
        // alert(`fetchedData : ${JSON.stringify(fetchedData)}`);
        if(fetchedData.length)
        {
          // temp.push(...fetchedData);
          for(let i of fetchedData)
          {
            temp.push(i["_source"]);
          }
          // console.log(`temp : ${JSON.stringify(temp)}`);
          return temp;
          // flatListSetter(temp);
          // setLatestNewsPageNo(latestNewsPageNo+1);
        }
        else
          return [];
            
    }

    useEffect(()=>{
      if(focusOnSearch)
      {
        // alert(`PRESSED!`);
        setRequestForData(true);
        setFocusOnSearch(false);
      }
    },[focusOnSearch])

    

    // let searchBtnRef = null;
    return (
      <SafeAreaView style={[styles.newsFeed]}>
        <View style={[styles.topNav]}>
          <TopNavigation appLanguage={appLanguage} navigation={props.navigation} />
        </View>
        <View style={{flex:0.9}}>
          <View style={{flex:0.1}}>
            <SearchInput focusNext={setFocusOnSearch} appLanguage={appLanguage} setSearchKey={setSearchKey} />
          </View> 
          <View style={{flex:0.9}}>
            <VideosFlatList LoadMoreData={LoadMoreData} appLanguage={appLanguage} queryData={requestForData} queryDataSetter={setRequestForData}/>
          </View>
        </View>
        
      </SafeAreaView>
    )
};

const styles=StyleSheet.create({
  newsFeed:{
      flex:1,
      // flexDirection:'row',
  },
  flatList:{
      flex:1
  },
  topNav: {
      flex: 0.1
  },
  searchBlck:{
    flex:0.85
  },
  searchTxtInput:{
    borderBottomWidth:2,
    alignSelf:'center',
    width:'90%',
    // flex:1,
    borderColor:'grey',
    // flex:0.9,
  },
  searchLogo:{
    // marginLeft:3,
    alignSelf:'center',
    marginTop:10,
  },
  loadingBlock:{
      // height:'90%',
      flex:1,
      // marginTop:'5%',
      width:(SCREEN_WIDTH*0.75),
      // borderRadius:20,    
      alignItems:'center',
      // backgroundColor:'yellow',
  },
  loadingIcon:{
      flex:0.99,
      width:'40%',
      alignItems:'center',
      // height:80,
      // backgroundColor:'red',
      resizeMode:'contain',
  },
});

export default Search;
