import React, {useEffect, useState} from 'react';
import {View, ActivityIndicator, Text, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';
import Toast from 'react-native-simple-toast';
// import {StackActions, NavigationActions} from '@react-navigation/native';
import { CommonActions } from '@react-navigation/native';

import { Auth } from '@aws-amplify/auth';
import Amplify from '@aws-amplify/core';
import awsConfig from '../../src/aws-exports';


const SignOutPage = (props)=>{
    const logoutUser = ()=>{
        Auth.signOut()
        .then(()=>{
            let resetAction = CommonActions.reset({
                index: 0,
                routes:[
                    {name:'Login',}
                ],
                // actions: [NavigationActions.navigate({ routeName: 'Login' })],
              });
            props.navigation.dispatch(resetAction);
        })
        .catch(e=>{
            Toast.show(`${JSON.stringify(e)}`)
        })
    }

    useEffect(()=>{
        logoutUser();
    });
    return (
        <View style={{flex:1, alignContent:'center', alignItems:'center', alignSelf:'center'}}>
            <ActivityIndicator style={{alignSelf:'center', alignItems:'center'}} size="large" color="#0000ff" />
            <Text style={{alignSelf:'center', alignItems:'center', fontSize:20}}>Logging You Out!</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    
});
export default SignOutPage;