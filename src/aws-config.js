// WARNING: DO NOT EDIT. This file is Auto-Generated by AWS Mobile Hub. It will be overwritten.

// Copyright 2017-2018 Amazon.com, Inc. or its affiliates (Amazon). All Rights Reserved.
// Code generated by AWS Mobile Hub. Amazon gives unlimited permission to
// copy, distribute and modify it.

// AWS Mobile Hub Project Constants
var aws_app_analytics = 'enable';
var aws_cognito_identity_pool_id = 'us-east-1:835ca292-ae72-4d2c-944c-c5c25c2d1577';
var aws_cognito_region = 'us-east-1';
var aws_mobile_analytics_app_id = 'e5c237670b174234923059ecb3c0df78';
var aws_mobile_analytics_app_region = 'us-east-1';
var aws_project_id = 'c188307e-3671-49c2-973d-0b2cffd835c0';
var aws_project_name = 'FrontEndLoginTest';
var aws_project_region = 'us-east-1';
var aws_resource_name_prefix = 'frontendlogintest-mobilehub-1699283815';
var aws_sign_in_enabled = 'enable';
var aws_user_pools = 'enable';
var aws_user_pools_id = 'us-east-1_Yq8XGhxkj';
var aws_user_pools_mfa_type = 'OFF';
var aws_user_pools_web_client_id = '1e70oii6tt9v6de8pbjdb9hdg4';

AWS.config.region = aws_project_region;
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: aws_cognito_identity_pool_id
  }, {
    region: aws_cognito_region
  });
AWS.config.update({customUserAgent: 'MobileHub v0.1'});
