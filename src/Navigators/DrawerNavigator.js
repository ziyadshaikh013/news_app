import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import { createDrawerNavigator } from '@react-navigation/drawer';
import ContactUs from './ContactUs';
import ChangeLang from './ChangeLang';
import EditInterest from './EditInterest';
import MyComments from './MyComments';
import PrivacyPolicy from './PrivacyPolicy';
import TermsCond from './TermsCond';
import CustomDrawerContent from '../components/Navigations/CustomDrawerContent';
import SignOutPage from './SignOutPage.js';
import TabNavigator from './TabNavigator';
import NewsFeed from './NewsFeed';

const MyDrawer = createDrawerNavigator();

const DrawerNavigator = () => {
    return (
        <MyDrawer.Navigator
            drawerContent={props => <CustomDrawerContent {...props} />}
            initialRouteName="Home"
            drawerContentOptions={{
                style: {
                    backgroundColor: '#F3F3F3',
                    paddingTop: 0,
                },
                activeTintColor: '#D44E68',
                labelStyle: {
                    fontFamily: 'open sans',
                    fontWeight: '700',
                    fontSize: 16,
                    textTransform: 'capitalize',
                },
            }}>
            <MyDrawer.Screen
                name="Home"
<<<<<<< HEAD:src/Navigators/DrawerNavigator.js
                component={TabNavigator}
=======
                component={NewsFeed}
>>>>>>> a46860955740381eb513550287b6b44a32b7c2da:src/screens/DrawerNavigator.js
                options={{
                    drawerLabel: 'Home',
                    drawerIcon: ({ color, size }) => <Icon name="home" color={color} size={size} />
                }}
            />
            <MyDrawer.Screen
                name="Edit Interests"
                component={EditInterest}
                options={{
                    drawerLabel: 'Edit Interests',
                    drawerIcon: ({ color, size }) => <Icon name="tasks" color={color} size={size} />
                }} />
            <MyDrawer.Screen
                name="Change Langauge"
                component={ChangeLang}
                options={{
                    drawerLabel: 'Change Language',
                    drawerIcon: ({ color, size }) => <Icon name="language" color={color} size={size} />
                }}
            />
            <MyDrawer.Screen
                name="My Comments"
                component={MyComments}
                options={{
                    drawerLabel: 'My Comments',
                    drawerIcon: ({ color, size }) => <Icon name="comments" color={color} size={size} />
                }}
            />
            <MyDrawer.Screen
                name="Contact Us"
                component={ContactUs}
                options={{
                    drawerLabel: 'Contact Us',
                    drawerIcon: ({ color, size }) => <Icon name="phone" color={color} size={size} />
                }}
            />
            <MyDrawer.Screen
                name="Terms & Conditions"
                component={TermsCond}
                options={{
                    drawerLabel: 'Terms & Condition',
                    drawerIcon: ({ color, size }) => <Icon name="file-text" color={color} size={size} />
                }}
            />
            <MyDrawer.Screen
                name="Privacy Policy"
                component={PrivacyPolicy}
                options={{
                    drawerLabel: 'Privacy Policy',
                    drawerIcon: ({ color, size }) => <Icon name="user" color={color} size={size} />
                }}
            />
            <MyDrawer.Screen
                name="Sign Out"
                component={SignOutPage}
                options={{
                    drawerLabel: 'Sign Out',
                    drawerIcon: ({ color, size }) => <Icon name="sign-out" color={color} size={size} />
                }}
            />
        </MyDrawer.Navigator>
    );
}

export default DrawerNavigator;