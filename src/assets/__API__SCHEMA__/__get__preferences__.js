

function getRequestObj(authToken)
{
    __GET__PREFERENCES__CATEGORIES__ = {
        method:'GET',
        headers:{
            'Accept':'application/json',
            'Content-Type':'application/json',
            'authorization':authToken,
            'cache-control':'no-cache',
        }
    };
    
};

function postRequestObj(authToken, data)
{
    __SUBMIT__PREFERENCES__= {
        method:'POST',
        headers:{
            'Accept':'application/json',
            'Content-Type':'application/json',
            'authorization':authToken,
            'cache-control':'no-cache',
        },
    };
}
module.exports.getRequestObj = getRequestObj;
module.exports.postRequestObj = postRequestObj;