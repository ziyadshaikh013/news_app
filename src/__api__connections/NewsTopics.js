import Toast from 'react-native-simple-toast';

import { Auth } from '@aws-amplify/auth';
import Amplify from '@aws-amplify/core';
import awsConfig from '../aws-exports';

let API_ENDPOINTS = require('../assets/__API__endpoints__/__backend__apis__urls__.js');

const getPrefferedTopics = async (topicId, pageNo, itemsPerPage)=>{
    let aa = (await Auth.currentSession()).getIdToken();
    return fetch(API_ENDPOINTS.GET_PREFERRED_NEWS(topicId, pageNo, itemsPerPage), {
        method:'GET',
        headers:{
            'Accept':'application/json',
            'Content-Type':'application/json',
            'Authorization':aa.jwtToken,
        }
    })
    .then(resp=>resp.json())
    .then(respData=>respData.response.data.videos)
    .catch(e=>{Toast.show(`${JSON.stringify(e)}`);});
};

const getLatestTopics = async (pageNo, itemsPerPage)=>{
    let aa = (await Auth.currentSession()).getIdToken();
    return fetch(API_ENDPOINTS.GET_NEWS_TOPICS(pageNo, itemsPerPage), {
        method:'GET',
        headers:{
            'Accept':'application/json',
            'Content-Type':'application/json',
            'Authorization':aa.jwtToken,
        }
    })
    .then(resp=>resp.json())
    .then(responseData=>responseData.response.data.videos)
    .catch(e=>{Toast.show(`${JSON.stringify(e)}`)});
};

module.exports.getPrefferedTopics = getPrefferedTopics;
module.exports.getLatestTopics = getLatestTopics;
