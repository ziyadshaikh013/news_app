import Toast from 'react-native-simple-toast';

import { Auth } from '@aws-amplify/auth';
import Amplify from '@aws-amplify/core';
import awsConfig from '../aws-exports';

let API_ENDPOINTS = require('../assets/__API__endpoints__/__backend__apis__urls__.js');



const BookmarkTheVideo = async (videoId)=>{
    let aa = (await Auth.currentSession()).getIdToken();
    return fetch(API_ENDPOINTS.BOOKMARK_VIDEO(), {
        method:'POST',
        headers:{
            'Accept':'application/json',
            'Content-Type':'application/json',
            'Authorization':aa.jwtToken,
        },
        body:JSON.stringify({
            "videoId" : videoId
        })
    })
    .then(resp=>resp.json())
    .then(responseData=>responseData.message)
    .catch(e=>{Toast.show(`${JSON.stringify(e)}`)});
};

const GetBookmarks = async(pageNo, itemsPerPage)=>{
    let aa = (await Auth.currentSession()).getIdToken();
    return fetch(API_ENDPOINTS.GET_BOOKMARKED_VIDEO(pageNo, itemsPerPage), {
        method:'GET',
        headers:{
            'Accept':'application/json',
            'Content-Type':'application/json',
            'Authorization':aa.jwtToken,
        },
    })
    .then(resp=>resp.json())
    .then(responseData=>responseData.response.data.videos)
    .catch(e=>{Toast.show(`${JSON.stringify(e)}`)});
}

// module.exports.getPrefferedTopics = getPrefferedTopics;
module.exports.BookmarkTheVideo = BookmarkTheVideo;
module.exports.GetBookmarks = GetBookmarks;