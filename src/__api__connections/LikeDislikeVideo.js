import Toast from 'react-native-simple-toast';

import { Auth } from '@aws-amplify/auth';
import Amplify from '@aws-amplify/core';
import awsConfig from '../aws-exports';

let API_ENDPOINTS = require('../assets/__API__endpoints__/__backend__apis__urls__.js');



const LikeDislikeVideo = async (videoId, userPref)=>{
    let aa = (await Auth.currentSession()).getIdToken();
    return fetch(API_ENDPOINTS.LIKE_DISLIKE_VIDEO(), {
        method:'POST',
        headers:{
            'Accept':'application/json',
            'Content-Type':'application/json',
            'Authorization':aa.jwtToken,
        },
        body:JSON.stringify({
            "videoId" : videoId,
            "userReaction":userPref
        })
    })
    .then(resp=>resp.json())
    .then(responseData=>responseData.message)
    .catch(e=>{Toast.show(`${JSON.stringify(e)}`)});
};

// module.exports.getPrefferedTopics = getPrefferedTopics;
module.exports.LikeDislikeVideo = LikeDislikeVideo;
