import Toast from 'react-native-simple-toast';
import { Auth } from '@aws-amplify/auth';
import Amplify from '@aws-amplify/core';
import awsConfig from '../aws-exports';
let API_ENDPOINTS = require('../assets/__API__endpoints__/__backend__apis__urls__.js');

const SearchForVideos = async (itemsPerPage, pageNo, searchString)=>{
    // let aa = (await Auth.currentSession()).getIdToken();
    return fetch(API_ENDPOINTS.SEARCH_VIDEOS(itemsPerPage, pageNo, searchString), {
        method:'GET',
        headers:{
            'Accept':'application/json',
            'Content-Type':'application/json',
            // 'Authorization':aa.jwtToken,
        }
    })
    .then(resp=>resp.json())
    .then(responseData=>responseData.hits.hits)
    .catch(e=>{Toast.show(`${JSON.stringify(e)}`)});
};
// module.exports.getPrefferedTopics = getPrefferedTopics;
module.exports.SearchForVideos = SearchForVideos;