import Toast from 'react-native-simple-toast';
import { Auth } from '@aws-amplify/auth';
import Amplify from '@aws-amplify/core';
import awsConfig from '../aws-exports';
let API_ENDPOINTS = require('../assets/__API__endpoints__/__backend__apis__urls__.js');
const GetUserTopics = async()=>{
    let aa = (await Auth.currentSession()).getIdToken();
    return fetch(API_ENDPOINTS.PREFERENCES(), {
        method:'GET',
        headers:{
            'Accept':'application/json',
            'Content-Type':'application/json',
            'Authorization':aa.jwtToken,
        }
    })
    .then(resp=>resp.json())
    .then(responseData=>responseData.response.userTopics)
    .catch(e=>{
        Toast.show(`${JSON.stringify(e)}`);
    });
}
const GetPrefferedTopics = async()=>{
  let aa = (await Auth.currentSession()).getIdToken();
  return fetch(API_ENDPOINTS.GET_CHOOSEN_TOPICS(), {
      method:'GET',
      headers:{
          'Accept':'application/json',
          'Content-Type':'application/json',
          'Authorization':aa.jwtToken,
      }
  })
  .then(resp=>resp.json())
  .then(responseData=>responseData.response)
  .catch(e=>{
      Toast.show(`${JSON.stringify(e)}`);
  });
}

const SubmitUserTopics = async(selObjDta)=>{
//   alert(`selObjDta : ${JSON.stringify(selObjDta)}`);
  let aa = (await Auth.currentSession()).getIdToken();
  return fetch(API_ENDPOINTS.UPDATE_PREFERENCES(), {
      method:'POST',
      headers:{
          'Accept':'application/json',
          'Content-Type':'application/json',
          'authorization':aa.jwtToken,
          'cache-control':'no-cache',
      },
      body:JSON.stringify({
        "selectedObjects":selObjDta
      })
    })
    .then(async resp=>resp.json())
    .then(respData=>respData)
    .catch(e=>{Toast.show(`${JSON.stringify(e)}`)});
}
module.exports.GetUserTopics = GetUserTopics;
module.exports.SubmitUserTopics = SubmitUserTopics;
module.exports.GetPrefferedTopics = GetPrefferedTopics;
