import Toast from 'react-native-simple-toast';
import { Auth } from '@aws-amplify/auth';
import Amplify from '@aws-amplify/core';
import awsConfig from '../aws-exports';
let API_ENDPOINTS = require('../assets/__API__endpoints__/__backend__apis__urls__.js');
const GetListedStates = async ()=>{
    let aa = (await Auth.currentSession()).getIdToken();
    return fetch(API_ENDPOINTS.GET_SAVED_STATES(), {
        method:'GET',
        headers:{
            'Accept':'application/json',
            'Content-Type':'application/json',
            'Authorization':aa.jwtToken,
        }
    })
    .then(resp=>resp.json())
    .then(responseData=>responseData.response)
    .catch(e=>{Toast.show(`${JSON.stringify(e)}`)});
};
const GetStateCities = async (state_id)=> {
    let aa = (await Auth.currentSession()).getIdToken();
    return fetch(API_ENDPOINTS.GET_STATE_CITIES(state_id), {
        method:'GET',
        headers:{
            'Accept':'application/json',
            'Content-Type':'application/json',
            'Authorization':aa.jwtToken,
        }
    })
    .then(resp=>resp.json())
    .then(responseData=>responseData.response)
    .catch(e=>{Toast.show(`${JSON.stringify(e)}`)});
}
const GetCitySuggestions = async search_key=>{
    let aa = (await Auth.currentSession()).getIdToken();
    return fetch(API_ENDPOINTS.SEARCH_CITIES(search_key), {
        method:'GET',
        headers:{
            'Accept':'application/json',
            'Content-Type':'application/json',
            'Authorization':aa.jwtToken,
        }
    })
    .then(resp=>resp.json())
    .then(responseData=>responseData.response)
    .catch(e=>{Toast.show(`${JSON.stringify(e)}`)});
};
// module.exports.getPrefferedTopics = getPrefferedTopics;
module.exports.GetListedStates = GetListedStates;
module.exports.GetStateCities = GetStateCities;
module.GetCitySuggestions = GetCitySuggestions;