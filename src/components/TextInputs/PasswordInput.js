import React, {useEffect} from 'react';
import {View, StyleSheet, Image, Text, TouchableOpacity} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import Icon from "react-native-vector-icons/SimpleLineIcons";

const data = require('../../assets/translations/texts.json');


const PasswordInput = (props) =>
{
    let passwordInputRef = null;
    useEffect(()=>{
        if(props.focusOnMe)
            passwordInputRef.focus();
    });
    return (
        <View style={[styles.passwordBtnView]}>
            <View style={[styles.textArea]}>
                <Icon name={"lock"} style={[styles.passwordLogo]}  color={'#dd4e68'} size={25} />

                <TextInput style={[styles.passwordTextInput]}
                    placeholder={props.placeHolderText}
                    ref={x=>passwordInputRef=x}
                    onSubmitEditing={()=>props.focusNext(true)}
                    onFocus={()=>props.focusNext(false)}
                    secureTextEntry={true}
                    onChangeText={(text)=>{props.setPassword(text)}}
                    returnKeyType={"next"}
                />
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    passwordBtnView:{
        flexDirection:'row',
        marginBottom:5
    },
    textArea:{
        // backgroundColor:'white',
        backgroundColor:'rgba(255,255,255,0.7)',
        flexDirection:'row',
        width:'80%',
        marginLeft:'10%',
        borderRadius:30  
      },
    passwordLogo:{
        alignSelf:'center',
        marginLeft:'5%',
        flex:0.15,
    },
    passwordTextInput:{
        color:'black',
        flex:0.85,
    }
});

export default PasswordInput;