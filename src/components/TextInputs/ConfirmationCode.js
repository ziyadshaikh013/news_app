import React from 'react';
import {View, StyleSheet, Image, Text, TouchableOpacity} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
const data = require('../../assets/translations/texts.json');

const ConfirmationCode = (props) =>
{
    return (
        <View style={{...styles.codeBtnView}}>
            <View style={{ ...styles.textArea}}>
                <Image 
                    style={{...styles.codeLogo}}
                    source={require("../../assets/images/keyIconBlackOrange.png")}
                />
                <TextInput style={{...styles.codeTextInput}}
                    placeholder={props.placeHolderText}
                    onChangeText={(text)=>{props.setCode(text)}}
                    keyboardType='numeric'
                />
            </View>
            {/* <View style={{flex:0.7}}>
                
            </View> */}
        </View>
    )
}
const styles = StyleSheet.create({
    codeBtnView:{
    },
    textArea:{
      backgroundColor:'white',
      flexDirection:'row',
      width:'80%',
    //   flex:1,
      marginLeft:'10%',
      borderRadius:30  
    },
    codeLogo:{
        // marginLeft:'5%',
        alignSelf:'center',
        height:40,
        width:40,
        flex:0.2,
        resizeMode:'contain'
    },
    codeTextInput:{
        color:'black',
        flex:0.7,
    }
});

export default ConfirmationCode;