import React, {useEffect} from 'react';
import {View, StyleSheet, Image, Text, TouchableOpacity} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import Icon from "react-native-vector-icons/MaterialIcons";

const data = require('../../assets/translations/texts.json');
let color='rgba(255,255,255,0.7)'

const NameInput = (props) =>
{
    let nameInputRef = null;
    useEffect(()=>{
        if(props.focusOnMe)
            nameInputRef.focus();
    })
    return (
        <View style={[styles.emailBtnView]}>
            <View style={[styles.textArea]}>
                <Icon name={"person-outline"} style={[styles.nameLogo]}  color={'#dd4e68'} size={30} />

                <TextInput style={[styles.nameTextInput]}
                    placeholder={props.placeHolderText}
                    onChangeText={(text)=>{props.setName(text)}}
                    returnKeyType={"next"}
                    ref={x=>nameInputRef=x}
                    onFocus={()=>props.focusNext(false)}
                    onSubmitEditing={()=>props.focusNext(true)}
                />
            </View>
            {/* <View style={{flex:0.7}}>
                
            </View> */}
        </View>
    )
}
const styles = StyleSheet.create({
    emailBtnView:{
    },
    textArea:{
    backgroundColor:color,
      flexDirection:'row',
      width:'80%',
      alignSelf:'center',
      borderRadius:30  
    },
    nameLogo:{
        marginLeft:'5%',
        alignSelf:'center',
        flex:0.15,
    },
    nameTextInput:{
        color:'black',
        flex:0.85,
    }
});

export default NameInput;