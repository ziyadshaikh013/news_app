import React, {useEffect, useRef, useState} from 'react';
import {View, StyleSheet, Image, Text, TouchableOpacity} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import Icon from "react-native-vector-icons/FontAwesome";

const data = require('../../assets/translations/texts.json');
let color='rgba(255,255,255,0.7)'

const EmailInput = (props) =>
{
    // const emailInputRef = useRef();
    let emailInputRef = null;
    useEffect(()=>{
        if(props.focusOnMe)
            emailInputRef.focus();
    });
    return (
        <View style={[styles.emailBtnView]}>
            <View style={[styles.textArea]}>
                <Icon name={"envelope-o"} style={[styles.emailLogo]}  color={'#dd4e68'} size={25} />

                <TextInput style={[styles.emailTextInput]}
                    placeholder={props.placeHolderText}
                    keyboardType={"email-address"}
                    ref={x=>emailInputRef=x}
                    // onKeyPress={{ nativeEvent: { key: 'enter' } }}
                    onFocus={()=>{props.focusNext(false);color='rgba(255,255,255,1)'}}
                    onSubmitEditing={()=>props.focusNext(true)}
                    onChangeText={(text)=>{props.setEmail(text)}}
                    returnKeyType={"next"}
                />
            </View>
            {/* <View style={{flex:0.7}}>
                
            </View> */}
        </View>
    )
}
const styles = StyleSheet.create({
    emailBtnView:{
    },
    textArea:{
      backgroundColor:color,
      flexDirection:'row',
      width:'80%',
      alignSelf:'center',
      borderRadius:30  
    },
    emailLogo:{
        marginLeft:'5%',
        alignSelf:'center',
        flex:0.15,
    },
    emailTextInput:{
        color:'black',
        flex:0.85,
    }
});

export default EmailInput;