
import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, TextInput, TouchableOpacity, FlatList, Dimensions, SafeAreaView, ScrollView, Image, ToastAndroid } from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";

const SCREEN_WIDTH = Dimensions.get('window').width;

const data = require('../../assets/translations/texts.json');

const SearchInput = props=>{
    return (
        <View style={{flex:1, flexDirection:'row'}}>
            <View style={[styles.searchBlck]}>
              <TextInput style={[styles.searchTxtInput]}
                placeholder={data["EnterTopicToSearch"][props.appLanguage]}
                onSubmitEditing={()=>props.focusNext(true)}
                onChangeText={text=>{props.setSearchKey(text)}}
                returnKeyType={"search"}
              />
            </View>
            <View style={{flex:0.15}}>
              <TouchableOpacity 
                // ref={inp=>searchBtnRef=inp}
                onPress={()=>props.focusNext(true)}
              >
              <Icon name={"search"} style={[styles.searchLogo]}  color={'#dd4e68'} size={30} />
              </TouchableOpacity>
            </View>
        </View>
    )
};

const styles=StyleSheet.create({
    newsFeed:{
        flex:1,
        // flexDirection:'row',
    },
    flatList:{
        flex:1
    },
    topNav: {
        flex: 0.1
    },
    searchBlck:{
      flex:0.85
    },
    searchTxtInput:{
      borderBottomWidth:2,
      alignSelf:'center',
      width:'90%',
      // flex:1,
      borderColor:'grey',
      // flex:0.9,
    },
    searchLogo:{
      // marginLeft:3,
      alignSelf:'center',
      marginTop:10,
    },
    loadingBlock:{
        // height:'90%',
        flex:1,
        // marginTop:'5%',
        width:(SCREEN_WIDTH*0.75),
        // borderRadius:20,    
        alignItems:'center',
        // backgroundColor:'yellow',
    },
    loadingIcon:{
        flex:0.99,
        width:'40%',
        alignItems:'center',
        // height:80,
        // backgroundColor:'red',
        resizeMode:'contain',
    },
  });

  export default SearchInput;