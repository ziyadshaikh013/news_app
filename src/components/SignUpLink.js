import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";

const data = require('../assets/translations/texts.json');

const SignUpLink = (props) => {
    return(
      <View style={{...styles.SignUpView}}>
          <TouchableOpacity onPress={()=>{props.navigateTo.navigate("Sign Up With Email")}} style={{...styles.SignUpBtn}}>
              <Text style={{...styles.SignUpText}}>
                  {data["SignUp"][props.appLanguage]}
              </Text>
          </TouchableOpacity>
      </View>
    )
}


const styles = StyleSheet.create({
  SignUpView: {
    flex: 1,
  },
  SignUpBtn: {
    flex: 1,
  },
  SignUpText:{
    color:'white',
    fontWeight:'bold',
    fontSize:16,
    // fontFamily:'' ,
    textAlign:'center',
    alignSelf:'center'
  }
});


export default SignUpLink;
