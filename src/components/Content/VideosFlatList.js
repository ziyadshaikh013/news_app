import React , {useState, useEffect} from 'react';
import {View, Text, StyleSheet, FlatList, Dimensions, SafeAreaView, ScrollView, Image, ToastAndroid } from 'react-native';


const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
import NewsBlock from './NewsBlock.js';
let NewsAPI = require('../../__api__connections/NewsTopics.js');
let API_ENDPOINTS = require('../../assets/__API__endpoints__/__backend__apis__urls__.js');
const data = require('../../assets/translations/texts.json');
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from "react-native-vector-icons/FontAwesome";


const VideosFlatList = props=>
{
    const [latestNewsData, setLatestNewsData] = useState([]);
    const [latestNewsPageNo, setLatestNewsPageNo] = useState(1);
    const [itemsPerPage, setItemsPerPage] = useState(10);
    const [currentVisibleVideo, setCurrentVisibleVideo] = useState(0);
    const [autoStartVideo, setAutoStartVideo] = useState(true);
    const [noSearchResultsFound, setNoSearchResultsFound] = useState(false);
    // const [isLoading, setIsLoading] = useState(false);
    // const [requestForData, setRequestForData] = useState(props.queryData);

    useEffect(()=>{
        if(props.queryData)
        {
            async function fetchData()
            {
                let d = await props.LoadMoreData(latestNewsData, latestNewsPageNo, itemsPerPage, 0);
                if(d.length)
                {
                    // alert(`d : ${JSON.stringify(d)}`);
                    setNoSearchResultsFound(false);
                    setLatestNewsPageNo(latestNewsPageNo+1);
                    setLatestNewsData(d);
                }
                else
                {
                    setNoSearchResultsFound(true);
                    setLatestNewsData([]);
                    setLatestNewsPageNo(0);
                }
            }
            fetchData();
            props.queryDataSetter(false);
        }
        if(props.clearPrevData)
        {
            setLatestNewsData({});
        }
    });
    const renderFooter = ()=>{
        
        return (
            props.queryData?<View style={[styles.loadingBlock]}>
                <Image
                    source={require('../../assets/animationGIFS/compass.gif')}
                    style={[styles.loadingIcon]}
                />
                <Text>Loading...</Text>
            </View>
            :
            null
        )
    };
    let flatListRef=null;

    const _goToNextPage = () => {
        flatListRef.scrollToIndex({
        index: currentVisibleVideo+1,
        animated: true,
       });
     };

    const scrollToFull = ()=>{
        if(flatListRef!=null)
        flatListRef.scrollToIndex({
            index:currentVisibleVideo,
            animated:true,
        })
    }
    useEffect(()=>{
        let timer;
        if(currentVisibleVideo>0)
        {
            timer = setTimeout(()=>scrollToFull(), 500);
        }
        return ()=>{
            clearTimeout(timer);
        }
    },[currentVisibleVideo]);
    
    const onViewRef = React.useRef((viewableItems)=> {
        if(viewableItems.viewableItems.length)
        {
            setCurrentVisibleVideo(viewableItems.viewableItems[0].index);
        }
    })
    const viewConfigRef = React.useRef({ viewAreaCoveragePercentThreshold: 50 })
    
    
    return (
        <SafeAreaView style={[styles.newsFeed]}>
            {latestNewsData.length?<FlatList
                // horizontal={true}
                ref={input=>flatListRef=input}
                style = {[styles.flatList]} 
                data={latestNewsData}
                onViewableItemsChanged={onViewRef.current}
                viewabilityConfig={viewConfigRef.current}
                onReachedThreshold={0.8}
                onEndReached={()=>props.LoadMoreData(latestNewsData, latestNewsPageNo, itemsPerPage, 1)}
                // onEndReachedThreshold={10}
                ListFooterComponent={renderFooter}
                renderItem={({item, index})=>{
                    return(
                        <NewsBlock 
                            autoStartVideo={autoStartVideo}
                            autoStartVideoSetter={setAutoStartVideo}
                            displayData={item}
                            myIndex={index}
                            isRowActive={true}
                            currentIndex={currentVisibleVideo}
                            scrollToNext = {_goToNextPage}
                        />
                    )
                }}
                keyExtractor={(item, index) => index.toString()} 
            />:
            noSearchResultsFound?
            <View style={{flex:1}}>
                <View style={{flex:0.5}}>
                    <MaterialCommunityIcons name={"emoticon-dead-outline"} style={[styles.searchLogo]}  color={'black'} size={105} />
                </View>
                <View style={{flex:0.5}}>
                <Text style={{fontSize:20, alignSelf:'center'}}>{data["NoVideosFound"][props.appLanguage]}</Text>
                </View>
            </View>
            :
            <View style={{flex:1}}>
                <View style={{flex:0.5}}>
                    <Icon name={"search"} style={[styles.searchLogo]}  color={'black'} size={105} />
                </View>
                <View style={{flex:0.5}}>
                <Text style={{fontSize:20, alignSelf:'center'}}>{data["SearchResultsText"][props.appLanguage]}</Text>
                </View>
            </View>
            }
        </SafeAreaView>
    );
}

const styles=StyleSheet.create({
    newsFeed:{
        flex:1,
        // flexDirection:'row',
    },
    flatList:{
        flex:1
    },
    searchLogo:{
        alignSelf:'center',
        marginTop:100,
    },
    loadingBlock:{
        // height:'90%',
        // marginTop:'5%',
        // width:(SCREEN_WIDTH*0.75),
        // borderRadius:20,    
        alignItems:'center',
        // backgroundColor:'yellow',
        // position: 'absolute',
        // width: '100%',
        // height: '100%',
        alignContent: 'center',
        // alignSelf:'center',
        // justifyContent: 'flex-end',
    },
    loadingIcon:{
        // flex:1,
        width:100,
        height:100,
        marginTop:'50%',
        // height:'100%',
        // marginLeft:'40%',
        // marginL
        alignSelf:'center',
        alignItems:'center',
        resizeMode:'contain',
        // borderRadius: 20,
        // backgroundColor: 'rgba(255, 255, 255, 0.3)'
    },
});

export default VideosFlatList;