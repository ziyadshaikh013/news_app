import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, PixelRatio, Dimensions, Platform, Image, StyleSheet } from 'react-native';

// import {WebView} from 'react-native-webview';
import YouTube from 'react-native-youtube';
import Icon from "react-native-vector-icons/AntDesign";
import { set } from 'react-native-reanimated';


const NewsVideoContent = (props) => {
  let youtubeRef = null;
  return (
    <View style={{ flex: 1, borderRadius: 10 }}>

      {props.videoIndexNo === props.activeIndexNo ?
        <YouTube
          ref={input => youtubeRef = input}
          apiKey="AIzaSyAq69kx2Yi3FEL67Wp2c1sfSfv6pXRvm60"
          videoId={props.videoUrl} // The YouTube video ID

          // ref={input=>props.videoRef(input)}
          play={props.videoIndexNo === props.activeIndexNo} // control playback of video with true/false
          controls={1}
          startTime={5}
          // onProgress={e=>setProgress(e)}
          onChangeState={e => { if (e.state === "ended") props.scrollToNext() }}
          // loop={()=>alert(`yo`)}
          fullscreen={false} // control whether the video should play in fullscreen or inline
          loop={false} // control whether the video should loop when ended
          onReady={e => youtubeRef.seekTo(props.skipTo)}
          // onChangeState={e => setPlayerState(e.state)}
          // onChangeQuality={e => setQuality(e.quality)}
          onError={e => { if (!props.intentionalOverlay) props.scrollToNext(); }}
          style={[styles.videoPlayer]}
        />
        :
        <View style={{ flex: 1 }}>
          <Image
            source={{ uri: props.thumbUrl }}
            style={[styles.videoThumbNail]}
          />
          <Icon name={"caretright"} style={[styles.playLogo]} color={'white'} size={45} />

        </View>
      }
    </View>
  )
}

const styles = StyleSheet.create({
  videoPlayer: {
    flex: 1,
    marginTop: '1%',
    // alignSelf:'stretch',
    // borderRadius:20,
    width: '96%',
    marginLeft: '2%',
    // margin:20,
  },
  videoThumbNail: {
    flex: 1,
  },
  playLogo: {
    position: 'absolute',
    alignSelf: 'center',
    alignContent: 'center',
    alignItems: 'center',
    marginTop: '35%',
  }
});

export default NewsVideoContent;
