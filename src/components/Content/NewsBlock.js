import React, { useEffect, useState } from 'react';
import {View, Text, Dimensions, StyleSheet, FlatList, TouchableOpacity, Image, WebView } from 'react-native';
import Toast from 'react-native-simple-toast';

let SCREEN_WIDTH = Dimensions.get('window').width;
let SCREEN_HEIGHT = Dimensions.get('window').height;

import PropTypes from 'prop-types';

import BtnGrps from '../shareBtns/BtnGrps.js';
import NewsVideoContent from './NewsVideoContent.js';

const BookmarkVideo = require('../../__api__connections/Bookmarks.js');
const LikeDislike = require('../../__api__connections/LikeDislikeVideo.js');

const NewsBlock = (props)=>
{
    const [videoBookmarked, setVideoBookmarked] = useState(false);
    const [videoLiked, setVideoLiked] = useState(false);
    const [videoDisliked, setVideoDisliked] = useState(false);

    const bookmarkVideo = ()=>{
        BookmarkVideo.BookmarkTheVideo(props.displayData.id).then(
            (resp)=>{
                if(resp==="Bookmark Added Successfully" || resp==="Video is already bookmarked")
                {
                    Toast.show(`Added to Bookmarks!`,0.6);
                    setVideoBookmarked(true);
                }
            }
        )
    };
    const convertDate = (timeStr)=>{
        var dateUTC = new Date(timeStr);
        var dateUTC = dateUTC.getTime() 
        var dateIST = new Date(dateUTC);
        //date shifting for IST timezone (+5 hours and 30 minutes)
        dateIST.setHours(dateIST.getHours() + 5); 
        dateIST.setMinutes(dateIST.getMinutes() + 30);
        
        return [`${dateIST.getDate()}-${dateIST.getMonth()}-${dateIST.getFullYear()}`, `${dateIST.getHours()}:${dateIST.getMinutes()}`]
    }
    const likeDislike = (userPref)=>{
        LikeDislike.LikeDislikeVideo(props.displayData.id, userPref)
        .then(
            resp=>{
                // alert(`resp : ${JSON.stringify(resp)}`);
                if(resp==="Liked Video"||resp==="Disliked Video")
                    if(userPref===2)
                    {
                        Toast.show(`Video Liked!`, 0.7);
                        setVideoLiked(true);
                        setVideoDisliked(false);

                    }
                    if(userPref===3)
                    {
                        Toast.show(`Video DisLiked!`, 0.7);
                        setVideoDisliked(true);
                        setVideoLiked(false);
                    }
            }
        )
    }
    console.log(`${JSON.stringify(props.displayData)}`)
    // useEffect(()=>{
    //     alert(`${startPlaying}`);
    //     if(isVisible === myIndex)
    //     {
    //         setStartPlaying(isVisible===myIndex)
    //     }
    // }, [isVisible]);
    return (
            <View style={[styles.newsBlock]}>
                <View style={[styles.newsThumbnail]}>
                    <NewsVideoContent 
                        thumbUrl={props.displayData.thumbUrl}
                        startVideo={props.autoStartVideo} 
                        videoUrl={props.displayData.youtubeVideoId}
                        skipTo={props.displayData.skipTime}
                        videoIndexNo={props.myIndex}
                        activeIndexNo={props.currentIndex}
                        isRowActive={props.isRowActive}
                        scrollToNext={props.scrollToNext}
                        intentionalOverlay = {props.intentionalOverlay}
                    />
                </View>
                <View style={[styles.newsTitleBox]}>
                    <View style={[styles.newsTitleBlock]}>
                        <View style={[styles.newsTitleTextBlock]}>
                            <Text style={[styles.newsTitleText]}>{props.displayData.videoTitle}</Text>

                        </View>
                        <View style={[styles.shareBtnsBlock]}>
                            <BtnGrps videoBookmarked={videoBookmarked} videoLiked={videoLiked} videoDisliked={videoDisliked} isAutoStartSet={props.autoStartVideo} bookmarkThis={bookmarkVideo} likeDislikeThisVideo={likeDislike} autostartVideoSetter={props.autoStartVideoSetter} />
                        </View>
                    </View>
                </View>
                <View style={[styles.dateTimeBox]}>
                    <View style={[styles.dateTimeTxtBox]}>
                        <View style={[styles.dateTxtView]}>
                            <Text style={[styles.dateTxt]}>{convertDate(props.displayData.publishedAt)[0]}</Text>
                        </View>
                        <View style={[styles.timeTxtView]}>
                            <Text style={[styles.timeTxt]}>{convertDate(props.displayData.publishedAt)[1]}</Text>
                        </View>
                    </View>
                </View>
            </View>
    );
}

NewsBlock.propTypes = {
    displayData:PropTypes.object
};

const styles=StyleSheet.create({
    newsBlock:{
        // flex:0.85,
        backgroundColor:'#1F1A1B',
        width:SCREEN_WIDTH*0.9,
        alignSelf:'center',
        height:SCREEN_HEIGHT*0.65,
        marginTop:10,
        marginBottom:10,
        // borderTopLeftRadius:20,
        // borderTopRightRadius:20,
        borderRadius:10,

    },  
    newsThumbnail:{
        flex:0.7,
        backgroundColor:'black',
        borderTopLeftRadius:20,
        borderTopRightRadius:20,
    },
  
    newsTitleBox:{
        flex:0.2,
    },
    newsTitleBlock:{
        flex:1,
        // flexDirection:'row',
    },
    newsTitleTextBlock:{
        flex:0.7,
        // alignContent:'center',
        // alignSelf:'center',
        // alignItems:'center',
        padding:10,
        // width:'90%',
    },
    newsTitleText:{
        fontSize:13,
        fontWeight:'bold',
        textAlign:'justify',
        padding:5,
        color:'white',
    },
    shareBtnsBlock:{
        flex:0.3,
        padding:10,
    }, 
   
    
    dateTimeBox:{
        flex:0.1,
    },
    dateTimeTxtBox:{
        flex:1,
        flexDirection:'row',
        padding:10,
    },
    dateTxtView:{
        flex:0.5,
    },
    dateTxt:{
        color:'white',
        fontWeight:'bold',
        alignSelf:'flex-start', 
        marginLeft:'5%',
    },
    timeTxtView:{
        flex:0.5,
    },
    timeTxt:{
        color:'white',
        alignSelf:'flex-end',
        fontWeight:'bold',
        marginRight:'5%',
    }
});

export default NewsBlock;