import React, {useState} from 'react';
import {View, Dimensions, Text, StyleSheet, ScrollView, FlatList, Image } from 'react-native';

import LatestNewsFlatList from './LatestNewsFlatList.js';
import NewsTypeSelection from '../Navigations/NewsTypeSelection.js';

let SCREEN_WIDTH = Dimensions.get('window').width;
let SCREEN_HEIGHT = Dimensions.get('window').height;


const NewsFeedContent = (props) =>
{
    const [activeTopic, setActiveTopic] = useState(-1);
    // const [fetchingInProgress, setFetchingInProgress] = useState(true);
    const [activeRow, setActiveRow] = useState(0);
    // const [rowNo, setRowNo] = useState(0);
    // useEffect(()=>{
        // if(props.intentionalOverLay)alert("YO");
    // });
    return (
        <React.Fragment>
            <View style={[styles.newsFeedType]}>
                    <NewsTypeSelection activateTopic={setActiveTopic}/>
            </View>

            {activeTopic===-1?
            <LatestNewsFlatList intentionalOverlay={props.intentionalOverLay} currentTopic={activeTopic} activeRow={activeRow} rowNo={0} />
            :
            <View style={[styles.newsFeedFlatList]}>
                <PrefferedNewsFlatList intentionalOverlay={props.intentionalOverLay} currentTopic={activeTopic} rowNo={0} activeRow={activeRow} />
            </View>}
        </React.Fragment>
        // <ScrollView style={[styles.newsFeed]}
        
        // >
        //     <View style={[styles.newsFeedFlatList]}>
        //         <LatestNewsFlatList activeRow={activeRow} rowNo={0} />
        //     </View>
        //     <View style={[styles.newsFeedType]}>
        //         <NewsTypeSelection activateTopic={setActiveTopic}/>
        //     </View>
        //     <View style={[styles.newsFeedFlatList1]}>
        //         <PrefferedNewsFlatList currentTopic={activeTopic} rowNo={1} activeRow={activeRow} />
        //     </View>
        // </ScrollView>
    );
}

const styles=StyleSheet.create({
    newsFeed:{
        // flex:1
    },
    newsFeedFlatList:{
        height:(SCREEN_HEIGHT*0.9),
        // flex:0.6
    },
    newsFeedType:{
        height:(SCREEN_HEIGHT*0.06),
    },
    newsFeedFlatList1:{
        // flex:0.4,
        // height:300
        height:(SCREEN_HEIGHT*0.6),
    }
});

export default NewsFeedContent;