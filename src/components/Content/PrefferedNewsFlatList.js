import React , {useState, useEffect} from 'react';
import {View, Text, StyleSheet, FlatList, Dimensions, SafeAreaView, ScrollView, Image, ToastAndroid } from 'react-native';
import Toast from 'react-native-simple-toast';
// let sampleData = require('../sampleData/newsFeed.json');

import { Auth } from '@aws-amplify/auth';
import Amplify from '@aws-amplify/core';
import awsConfig from '../../../src/aws-exports';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
import NewsBlock from './NewsBlock.js';

const NewsAPI = require('../../__api__connections/NewsTopics.js');


async function LoadMoreData(flatListData, flatListSetter, latestNewsPageNo, setLatestNewsPageNo, itemsPerPage, topicId, dataNotFound)
{
    // alert(`called rerender`);
    console.log(`topicId : ${topicId}, itemsPerPage : ${itemsPerPage}, latestNewsPageNo : ${latestNewsPageNo}`);
    let temp = flatListData;
    let fetchedData = await NewsAPI.getPrefferedTopics(topicId, latestNewsPageNo, itemsPerPage);
    console.log(`topicId : ${topicId},  fetchedData : ${JSON.stringify(fetchedData)}`);
    if(fetchedData.length)
    {
        temp.push(...fetchedData);
        flatListSetter(temp);
        setLatestNewsPageNo(latestNewsPageNo+1);
    }
    else
        dataNotFound(true);
        
}
const PrefferedNewsFlatList = props=>
{
    // alert(`response : ${JSON.stringify(sampleData.response[0]["thumbUrl"])}`)
    const [latestNewsData, setLatestNewsData] = useState([]);
    const [loadedInitially, setLoadedInitially] = useState(false);
    const [latestNewsPageNo, setLatestNewsPageNo] = useState(1);
    const [itemsPerPage, setItemsPerPage] = useState(10);
    const [allDataFetched, setAllDataFetched] = useState(false);
    const [currentVisibleVideo, setCurrentVisibleVideo] = useState(0);
    const [currentlyActiveTopic, setCurrentlyActiveTopic] = useState(props.currentTopic);
    const [noDataFound, setNoDataFound] = useState(false);
    const [autoStartVideo, setAutoStartVideo] = useState(true);

    useEffect(()=>{
        console.log(`settingCurrentlyActiveTopic props.currentTopic :${props.currentTopic}, currentlyActiveTopic : ${currentlyActiveTopic}`);
        if(currentlyActiveTopic!==props.currentTopic)
        {
            setCurrentlyActiveTopic(props.currentTopic);
            setLoadedInitially(false);
            setNoDataFound(false);
            setLatestNewsPageNo(1);
            setLatestNewsData([]);
        }
        
    });
    useEffect(()=>{
        if(!loadedInitially&&currentlyActiveTopic>=0)
        {
            console.log(`loadingMoreData`);
            LoadMoreData(latestNewsData, setLatestNewsData, latestNewsPageNo, setLatestNewsPageNo, itemsPerPage, currentlyActiveTopic, setNoDataFound);
            setLoadedInitially(true);
        }
    }, [currentlyActiveTopic]);
    const renderFooter = ()=>{
         if(noDataFound)
        return (
         <View style={[styles.loadingBlock]}>
             <Text style={{fontSize:20, alignItems:'center', alignSelf:'center', alignContent:'center'}}>No News Found For This Topic!</Text>
         </View>   
        )
        else
        return (
            <View style={[styles.loadingBlock]}>
                <Image
                    source={require('../../assets/animationGIFS/compass.gif')}
                    style={[styles.loadingIcon]}
                />
                <Text>Loading...</Text>
            </View>
        )
        
        
    };
    const onViewRef = React.useRef((viewableItems)=> {
        if(viewableItems.viewableItems.length)
        setCurrentVisibleVideo(viewableItems.viewableItems[0].index);
    })
    const viewConfigRef = React.useRef({ viewAreaCoveragePercentThreshold: 50 })
  
    const onViewableItemChanged = ({viewableItem, changed})=>{
        // console.log(`YOYO : ${viewableItem[0].index}`);
    };
    let flatListRef=null;
    
    const _goToNextPage = () => {
        if (currentVisibleVideo >= latestNewsData.length-1) currentVisibleVideo = 0;
        flatListRef.scrollToIndex({
        index: currentVisibleVideo+1,
        animated: true,
       });
     };
     const scrollToFull = ()=>{
        if(flatListRef!=null)
        flatListRef.scrollToIndex({
            index:currentVisibleVideo,
            animated:true,
        })
    };
    useEffect(()=>{
        let timer;
        if(currentVisibleVideo>0)
        {
            timer = setTimeout(()=>scrollToFull(), 500);
        }
        return ()=>{
            clearTimeout(timer);
        }
    },[currentVisibleVideo]);
    return (
        <SafeAreaView style={[styles.newsFeed]}>
            <FlatList
                // horizontal={true}
                ref={input=>flatListRef=input}
                style = {[styles.flatList]} 
                data={latestNewsData}
                onViewableItemsChanged={onViewRef.current}
                viewabilityConfig={viewConfigRef.current}
                onReachedThreshold={0.8}
                onEndReached={()=>LoadMoreData(latestNewsData, setLatestNewsData, latestNewsPageNo, setLatestNewsPageNo, itemsPerPage, currentlyActiveTopic, setNoDataFound)}
                ListFooterComponent={renderFooter}
                renderItem={({item, index})=>{
                    return( <NewsBlock
                        displayData={item}
                        autoStartVideo={autoStartVideo} 
                        autoStartVideoSetter={setAutoStartVideo}
                        myIndex={index}
                        isRowActive={false}
                        currentIndex={currentVisibleVideo}
                        scrollToNext = {_goToNextPage}/>
                        )
                        
                }}
                keyExtractor={(item, index) => index.toString()} 
            />
        </SafeAreaView>
    );
}

const styles=StyleSheet.create({
    newsFeed:{
        flex:1,
        // flexDirection:'row',
    },
    flatList:{
        flex:1
    },
    loadingBlock:{
        // height:'90%',
        // marginTop:'5%',
        // width:(SCREEN_WIDTH*0.75),
        // borderRadius:20,    
        alignItems:'center',
        // backgroundColor:'yellow',
        // position: 'absolute',
        // width: '100%',
        // height: '100%',
        alignContent: 'center',
        // alignSelf:'center',
        // justifyContent: 'flex-end',
    },
    loadingIcon:{
        // flex:1,
        width:100,
        height:100,
        marginTop:'50%',
        // height:'100%',
        // marginLeft:'40%',
        // marginL
        alignSelf:'center',
        alignItems:'center',
        resizeMode:'contain',
        // borderRadius: 20,
        // backgroundColor: 'rgba(255, 255, 255, 0.3)'
    },
});

export default PrefferedNewsFlatList;