import React , {useState, useEffect} from 'react';
import {View, Text, StyleSheet, FlatList, Dimensions, SafeAreaView, ScrollView, Image, ToastAndroid } from 'react-native';


const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
import NewsBlock from './NewsBlock.js';

let API_ENDPOINTS = require('../../assets/__API__endpoints__/__backend__apis__urls__.js');

const getNewsFeed = async(pageNo, itemsPerPage)=>{
    // setContentIsLoading(true);
    let aa = (await Auth.currentSession()).getIdToken();
    return fetch(API_ENDPOINTS.GET_NEWS_TOPICS(pageNo, itemsPerPage), {
        method:'GET',
        headers:{
            'Accept':'application/json',
            'Content-Type':'application/json',
            'Authorization':aa.jwtToken,
        }
    })
    .then(resp=>resp.json())
    .then(responseData=>responseData.response.data.videos);
};

async function LoadMoreData(flatListData, flatListSetter, latestNewsPageNo, setLatestNewsPageNo, itemsPerPage)
{
    let temp = flatListData;
    let fetchedData = await NewsAPI.getLatestTopics(latestNewsPageNo, itemsPerPage);
    console.log(`fetchedData : ${JSON.stringify(fetchedData)}`);
    if(fetchedData.length)
    {
        temp.push(...fetchedData);
        flatListSetter(temp);
        setLatestNewsPageNo(latestNewsPageNo+1);
    }
        
}
const LatestNewsFlatList = props=>
{
    // alert(`response : ${JSON.stringify(sampleData.response[0]["thumbUrl"])}`)
    const [latestNewsData, setLatestNewsData] = useState([]);
    const [loadedInitially, setLoadedInitially] = useState(false);
    const [latestNewsPageNo, setLatestNewsPageNo] = useState(1);
    const [itemsPerPage, setItemsPerPage] = useState(10);
    const [currentVisibleVideo, setCurrentVisibleVideo] = useState(0);
    const [autoStartVideo, setAutoStartVideo] = useState(true);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(()=>{
        if(!loadedInitially)
        {
            LoadMoreData(latestNewsData, setLatestNewsData, latestNewsPageNo, setLatestNewsPageNo, itemsPerPage);
            setLoadedInitially(true);
        }
        return (()=>{
            
        });
    },[loadedInitially]);
    
    const renderFooter = ()=>{
        return (
            <View style={[styles.loadingBlock]}>
                <Image
                    source={require('../../assets/animationGIFS/compass.gif')}
                    style={[styles.loadingIcon]}
                />
                <Text>Loading...</Text>
            </View>
        )
    };
    let flatListRef=null;

    const _goToNextPage = () => {
        flatListRef.scrollToIndex({
        index: currentVisibleVideo+1,
        animated: true,
       });
     };

    const scrollToFull = ()=>{
        if(flatListRef!=null)
        flatListRef.scrollToIndex({
            index:currentVisibleVideo,
            animated:true,
        })
    }
    useEffect(()=>{
        let timer;
        if(currentVisibleVideo>0)
        {
            timer = setTimeout(()=>scrollToFull(), 500);
        }
        return ()=>{
            clearTimeout(timer);
        }
    },[currentVisibleVideo]);
    
    const onViewRef = React.useRef((viewableItems)=> {
        if(viewableItems.viewableItems.length)
        {
            setCurrentVisibleVideo(viewableItems.viewableItems[0].index);
        }
    })
    const viewConfigRef = React.useRef({ viewAreaCoveragePercentThreshold: 50 })
    
    
    return (
        <SafeAreaView style={[styles.newsFeed]}>
            <FlatList
                // horizontal={true}
                ref={input=>flatListRef=input}
                style = {[styles.flatList]} 
                data={latestNewsData}
                onViewableItemsChanged={onViewRef.current}
                viewabilityConfig={viewConfigRef.current}
                onReachedThreshold={0.8}
                onEndReached={()=>LoadMoreData(latestNewsData, setLatestNewsData, latestNewsPageNo, setLatestNewsPageNo, itemsPerPage)}
                // onEndReachedThreshold={10}
                ListFooterComponent={renderFooter}
                renderItem={({item, index})=>{
                    return(
                        <NewsBlock 
                            autoStartVideo={autoStartVideo}
                            autoStartVideoSetter={setAutoStartVideo}
                            displayData={item}
                            myIndex={index}
                            isRowActive={true}
                            currentIndex={currentVisibleVideo}
                            scrollToNext = {_goToNextPage}
                            intentionalOverlay = {props.intentionalOverlay}
                        />
                    )
                }}
                keyExtractor={(item, index) => index.toString()} 
            />
            {isLoading && 
                <View style={[styles.loadingBlock]}>
                    <Image
                        source={require('../../assets/animationGIFS/compass.gif')}
                        style={[styles.loadingIcon]}
                    />
                </View>
            }
        </SafeAreaView>
    );
}

const styles=StyleSheet.create({
    newsFeed:{
        flex:1,
        // flexDirection:'row',
    },
    flatList:{
        flex:1
    },
    loadingBlock:{
        // height:'90%',
        // marginTop:'5%',
        // width:(SCREEN_WIDTH*0.75),
        // borderRadius:20,    
        alignItems:'center',
        // backgroundColor:'yellow',
        // position: 'absolute',
        // width: '100%',
        // height: '100%',
        alignContent: 'center',
        // alignSelf:'center',
        // justifyContent: 'flex-end',
    },
    loadingIcon:{
        // flex:1,
        width:100,
        height:100,
        marginTop:'50%',
        // height:'100%',
        // marginLeft:'40%',
        // marginL
        alignSelf:'center',
        alignItems:'center',
        resizeMode:'contain',
        // borderRadius: 20,
        // backgroundColor: 'rgba(255, 255, 255, 0.3)'
    },
});

export default LatestNewsFlatList;