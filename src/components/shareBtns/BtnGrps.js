import React, {useEffect} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";
import IoniconsI from 'react-native-vector-icons/Ionicons'

// import { BOOKMARK_VIDEO } from '../../assets/__API__endpoints__/__backend__apis__urls__';

const BtnGrps = (props) =>
{
    let iconSize=25;
    let btnColor='#c2c2c2';
    let btnSelColor = "#C25C70";
    return (
        <View style={[styles.shareBtnGrp]}>
            <View style={[styles.BtnBlck]}>
                <TouchableOpacity onPress={()=>props.likeDislikeThisVideo(2)}>
                    {props.videoLiked?
                        <Icon name={"thumbs-up"} style={[styles.btnImg]}  color={btnSelColor} size={iconSize} />

                    :
                        <Icon name={"thumbs-o-up"} style={[styles.btnImg]}  color={btnColor} size={iconSize} />
                    }
                </TouchableOpacity>
            </View>
            <View style={[styles.BtnBlck]}>
                <TouchableOpacity onPress={()=>props.likeDislikeThisVideo(3)}>
                    {props.videoDisliked?
                        <Icon name={"thumbs-down"} style={[styles.btnImg]}  color={btnSelColor} size={iconSize} />
                    :
                        <Icon name={"thumbs-o-down"} style={[styles.btnImg]}  color={btnColor} size={iconSize} />
                    }

                </TouchableOpacity>
            </View>

            <View style={[styles.BtnBlck]}>
                <TouchableOpacity onPress={()=>props.bookmarkThis()}>
                {props.videoBookmarked?
                    <Icon name={"bookmark"} style={[styles.btnImg]}  color={btnSelColor} size={iconSize} />
                    :
                    <Icon name={"bookmark-o"} style={[styles.btnImg]}  color={btnColor} size={iconSize} />
                }
                </TouchableOpacity>
            </View>

            <View style={[styles.BtnBlck]}>
                <TouchableOpacity >
                   
                    <Icon name={"share"} style={[styles.btnImg]}  color={btnColor} size={iconSize} />

                </TouchableOpacity>
            </View>
            
            
            
            <View style={[styles.BtnBlck]}>
                <TouchableOpacity>
                        <IoniconsI name={"logo-whatsapp"} style={[styles.btnImg]}  color={btnColor} size={iconSize} />
                </TouchableOpacity>
            </View>
        </View>
    )
};


const styles = StyleSheet.create({
    shareBtnGrp:{
        flex:1,
        flexDirection:'row',
        // width:'95%'
        // backgroundColor:'blue',
    },
    Btn1Blck:{
        flex:0.25,
        alignSelf:'center',
    },
    Btn2Blck:{
        flex:0.25,
        // alignItems:'center',
        alignSelf:'center',
        // alignContent:'center'
    },
    Btn3Blck:{
        flex:0.25,
        // alignItems:'center',
        alignSelf:'center',
        // alignContent:'center'
    },
    BtnBlck:{
        flex:0.20,
        // alignItems:'center',
        alignSelf:'center',

        // backgroundColor:'#CFCFCF',
        padding:5,
        margin:2,
        borderRadius:5
    },
    Btn4Blck:{
        
        // alignContent:'center'
    },
    btnImg:{
        alignItems:'center',
        alignSelf:'center',
        alignContent:'center'
    },
});

export default BtnGrps;