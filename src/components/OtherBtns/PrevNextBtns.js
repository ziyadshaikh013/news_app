import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import Icons from 'react-native-vector-icons/FontAwesome'

const data = require('../../assets/translations/texts.json');

const PrevNextBtns = (props) =>
{
    return (
        <View style={styles.navigationBtnBlck}>

          {props.activatePrevious?<View style={styles.prevBtnBlck}>
            <TouchableOpacity onPress={()=>{props.PreviousCallBack()}} style={styles.prevBtn}>
              <Text style={styles.prevBtnTxt}><Icons name="angle-left" color="white" size={18} /> &nbsp; {data["Previous"][props.appLanguage]}</Text>
            </TouchableOpacity>
          </View>:null}

          {props.activeNext?<View style={styles.nextBtnBlck}>
            <TouchableOpacity onPress={()=>{props.NextCallback()}} style={styles.nextBtn}>
              <Text style={styles.nextBtnTxt}>{data["Next"][props.appLanguage]}&nbsp; <Icons name="angle-right" color="white" size={18} /></Text>
            </TouchableOpacity>
          </View>:null}
        </View>
    )
}

const styles = StyleSheet.create({
    navigationBtnBlck:{
        flexDirection:'row',
      },
      prevBtnBlck:{
        alignSelf:'center',
        flex:1,
      },
      prevBtn:{
        height: 40,
        alignSelf:'flex-start',
        padding:20,
        backgroundColor:'#d55164',
        borderRadius:10,
        marginLeft:20,
        alignItems: 'center',
        justifyContent: 'center'
      },
      prevBtnTxt:{
        color:'white',
        fontSize:15,
      },
      nextBtnBlck:{
        alignSelf:'center',
        flex:1,
      },
      nextBtn:{
        height: 40,
        alignSelf:'flex-end',
        padding:20,
        backgroundColor:'#d55164',
        borderRadius:10,
        marginRight:20,
        alignItems: 'center',
        justifyContent: 'center'
      },
      nextBtnTxt:{
          color:'white',
          fontSize:15
      }
});

export default PrevNextBtns;