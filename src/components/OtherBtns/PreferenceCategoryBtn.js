import React,  {useState, useEffect} from 'react';
import {View, Text, StyleSheet, Image, Dimensions} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from "react-native-vector-icons/FontAwesome";


let SCREEN_WIDTH = Dimensions.get('window').width;
let SCREEN_HEIGHT = Dimensions.get('window').height;

const PreferenceCategoryBtn = (props) =>
{
    const [isSelected, setIsSelected] = useState(props.displayData.isSelected);
    
    return (
        <View style={styles.categoryBtnBlck}>
            <TouchableOpacity onPress={()=>{isSelected?setIsSelected(false):setIsSelected(true);props.HandleSelection(props.displayData.topicId)}}>
                <View style={styles.categoryBlck}>
                    <View style={styles.categoryImgBlck}>
                        <Image
                            source={{uri:props.displayData.icon}}
                            style={styles.thumbnailImg}
                        />
                    </View>
                    <View style={styles.categoryNameBlck}>
                        <Text style={styles.categoryNameText}>{props.displayData.title}</Text>
                    </View>
                    
                </View>
                {isSelected &&
                    <View style={styles.selectedOverlay}>
                            {/* <Image 
                                source={require('../../assets/images/tickIconGreen.png')}
                                style={styles.categorySelectedIcon}
                            /> */}
                            <Icon name={"check-circle"} style={[styles.categorySelectedIcon]}  color={'green'} size={15} />
                    </View>
                    }
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    categoryBtnBlck:{
        flex: 1/3,
        height: SCREEN_HEIGHT/4,
        padding: 3,
    },
    categorySelectedIcon:{
        // height:100,
        // width:100,
        alignSelf:'flex-end',
        marginTop:'10%',
        marginRight:'7%'

        // alignItems: "center",
        // justifyContent: 'center'
    },
    categoryBlck:{
        paddingVertical: 20,
        height: '100%',
        width: '100%',
        backgroundColor:'#dddddd',
        borderRadius:30,
        position: 'relative',
        overflow: 'hidden',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    categoryImgBlck:{
        alignSelf:'center',
        height: "60%",
        width: "60%",
        overflow: 'hidden'
    },
    selectedOverlay:{
        position:'absolute',
        height: '100%',
        width: '100%',
        borderRadius:30,
        backgroundColor:'rgba(228,71,104,0.4)',
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center',
    },
    categoryNameBlck:{
        alignSelf:'center',
        alignItems:'center',
    },
    categoryNameText:{
        fontSize:14
    },
    thumbnailImg:{
        flex: 1,
        resizeMode:'contain'
    }
})

export default PreferenceCategoryBtn;