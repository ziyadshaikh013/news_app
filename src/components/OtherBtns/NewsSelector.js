import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Dimensions} from 'react-native';

let SCREEN_WIDTH = Dimensions.get('window').width;
let SCREEN_HEIGHT = Dimensions.get('window').height;

const NewsSelector = (props) =>
{
    return (
        <View style={[styles.newsSelectorBtn]}>
            {
            props.isActive==props.displayData.topicId?
            <TouchableOpacity>
                <View style={[styles.newsSelectorBtnBlckActive]}>
                    <Text style={[styles.newsSelectorBtnActive]}>{props.displayData.title}</Text>
                </View>
            </TouchableOpacity>
            :
            <TouchableOpacity onPress={()=>props.activate(props.displayData.topicId)}>
                <View style={[styles.newsSelectorBtnBlck]}>
                    <Text style={[styles.newsSelectorBtnInactive]}>{props.displayData.title}</Text>
                </View>
            </TouchableOpacity>
            
            }
        </View>
    )
}

const styles = StyleSheet.create({
    newsSelectorBtn:{
        // flex:1
        // width: SCREEN_WIDTH*0.22
    },
    newsSelectorBtnBlck:{
        // flex:1
        marginLeft:20,
        marginTop:11,
        // backgroundColor:'#dbd6d6',
        // backgroundColor:'white',
        // marginLeft:'1%'
        borderColor:'grey',
        // borderBottomWidth:2,
        // borderRadius:30,
        
        paddingLeft:10,
        paddingRight:10,
        // alignItems:'center',
    },
    newsSelectorBtnBlckActive:{
        // flex:1
        marginLeft:20,
        // backgroundColor:'white',

        marginTop:11,
        // marginLeft:'1%'
        borderColor:'#910707',
        borderBottomWidth:3,
        // borderRadius:30,

        paddingLeft:10,
        paddingRight:10,
        // alignItems:'center',
    },
    newsSelectorBtnTxtActve:{
        // color:'grey',
        color:'#910707',
        fontWeight:'bold',
        fontSize:20,
        textAlign:'center',
        alignSelf:'center',
    },
    newsSelectorBtnInactive:{
        // color:'red',
        color:'grey',
        fontSize:20,
        opacity:0.8,
        textAlign:'center',
        alignSelf:'center',
    }
});

export default NewsSelector;