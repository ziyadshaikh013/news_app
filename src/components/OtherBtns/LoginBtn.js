import React, { Component, useEffect } from "react";
import { StyleSheet, View, Image, Text, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";

const data = require('../../assets/translations/texts.json');
const LoginBtn = (props) => {
    let btnRef=null;
    useEffect(()=>{
        if(props.focusOnMe)
            props.submitBtn();
            // btnRef.touchableHandlePress();
    })
    return(
        <View style={styles.emailBtnView}>
            <TouchableOpacity onPress={() => { props.submitBtn() }} ref={x=>btnRef=x}>
                <View style={[styles.btnView]}>
                    <Icon name="sign-in" color={'white'} size={25} />
                    <Text style={[styles.btnText]}>
                        {data["Login"][props.appLanguage]}
                    </Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}


const styles = StyleSheet.create({
    emailBtnView: {
        flex: 1,
        alignItems: 'center'
    },
    btnView: {
        height: 50,
        flexDirection: 'row',
        backgroundColor: '#dd4e68',
        borderRadius: 50,
        width: 280,
        paddingHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnText: {
        fontSize: 15,
        alignSelf: 'center',
        alignContent: 'center',
        color: 'white',
        marginHorizontal: 10,
        textTransform: 'uppercase',
        fontWeight:'bold',
        fontSize: 16,
    }
});


export default LoginBtn;
