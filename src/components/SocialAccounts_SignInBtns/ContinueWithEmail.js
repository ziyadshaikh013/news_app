import React from 'react';
import {View, StyleSheet, Image, Text, TouchableOpacity} from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";


const data = require('../../assets/translations/texts.json');

const ContinueWithEmail = (props) => {
    return (
        <View style={[styles.emailBtnView]}>
            <TouchableOpacity onPress={ ()=> props.navigateTo.navigate('Login With Email') }>
                <View style={[styles.btnView]}>
                    <View style={[styles.btnTxt]}>
                        <Icon name={"envelope-o"} style={[styles.emailLogo]}  color={'white'} size={25} />
                        <Text style={[styles.btnText]}>
                            {data["LoginWith_email"][props.appLanguage]}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    )
}
const styles = StyleSheet.create({
    emailBtnView:{
        flex:1,
    },
    emailLogo:{
        marginLeft:'15%',
        marginRight:'4%',
        alignSelf:'center',
    },
    btnView:{
        flexDirection:'row',
        backgroundColor:'#d44e68',
        borderRadius:30,
        height:50,
        width:'70%',
        alignContent:'center',
        alignItems:'center',
        alignSelf:'center',
    },
    btnText:{
        fontSize:15,
        alignSelf:'center',
        alignContent:'center',
        color:'white'
    },
    btnTxt:{
        flexDirection:'row', 
        alignSelf:'center',
        alignItems:'center',
        // flex:1,
        alignContent:'center',
        
    },
});

export default ContinueWithEmail;