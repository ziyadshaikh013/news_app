import React from 'react';
import { View, StyleSheet, Image, Text, TouchableOpacity } from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";

const data = require('../../assets/translations/texts.json');

const ContinueWithGoogle = (props) => {
    return (
        <View style={[styles.googleBtnView]}>
            <TouchableOpacity onPress={() => props.signInCallback()}>
                <View style={[styles.btnView]}>
                    <View style={[styles.btnTxt]}>
                        <Icon name={"google"} style={[styles.googleLogo]} color={'black'} size={30} />
                        <Text style={[styles.btnText]}>
                            {data["LoginWith_google"][props.appLanguage]}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    googleLogo: {
        marginLeft: '10%',
        marginRight: '4%',
        alignSelf: 'center',
    },
    btnView: {
        flexDirection: 'row',
        backgroundColor: 'white',
        borderRadius: 30,
        height: 50,
        width: '70%',
        alignContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    btnText: {
        fontSize: 15,
        alignSelf: 'center',
        alignContent: 'center',
        color: 'black'
    },
    btnTxt: {
        flexDirection: 'row',
        alignSelf: 'center',
        alignItems: 'center',
        // flex:1,
        // fontSize:20,
        alignContent: 'center',

    },
});

export default ContinueWithGoogle;