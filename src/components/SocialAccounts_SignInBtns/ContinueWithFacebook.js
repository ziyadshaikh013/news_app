import React from 'react';
import {View, StyleSheet, Image, Text, TouchableOpacity} from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";

const data = require('../../assets/translations/texts.json');

const ContinueWithFacebook = (props) => {
    return (
        <View style={[styles.facebookBtnView]}>
            <TouchableOpacity>
                <View style={[styles.btnView]}>
                    <View style={[styles.btnTxt]}>
                        <Icon name={"facebook"} style={[styles.facebookLogo]}  color={'white'} size={25} />
                        <Text style={[styles.btnText]}>
                            {data["LoginWith_facebook"][props.appLanguage]}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    facebookLogo:{
        marginLeft:'14%',
        marginRight:'4%',
        alignSelf:'center',
    },
    btnView:{
        flexDirection:'row',
        backgroundColor:'#4267B2',
        borderRadius:30,
        height:50,
        width:'70%',
        alignContent:'center',
        alignItems:'center',
        alignSelf:'center',
    },
    btnText:{
        fontSize:15,
        alignSelf:'center',
        alignContent:'center',
        color:'white'
    },
    btnTxt:{
        flexDirection:'row', 
        alignSelf:'center',
        alignItems:'center',
        // flex:1,
        alignContent:'center',
        
    },
});

export default ContinueWithFacebook;