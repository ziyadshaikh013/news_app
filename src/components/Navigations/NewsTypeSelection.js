import React, { useState, useEffect } from 'react';
import { View, StyleSheet, SafeAreaView, FlatList, Text, Image, Dimensions } from 'react-native';

import NewsSelector from '../OtherBtns/NewsSelector.js';
const API_ENDPOINTS = require('../../assets/__API__endpoints__/__backend__apis__urls__.js');
const UserTopics = require('../../__api__connections/UserTopics.js');



const NewsTypeSelection = (props) => {
    const [flatListData, setFlatListData] = useState([]);
    useEffect(() => {

    });
    const [currentlyActive, setCurrentlyActive] = useState(-1);

    const [fetchingInProgress, setFetchingInProgress] = useState(true);
    // const []
    const fetchCategoriesData = async () => {
        let fetchedData = await UserTopics.GetPrefferedTopics();
        fetchedData = [{ "topicId": -1, "title": "Latest News", "icon": "https://updatedevprototype.s3.ap-south-1.amazonaws.com/Topic_Images/General+News.png", "isSelected": true }, ...fetchedData]
        // alert(`${JSON.stringify(fetchedData)}`);
        setFlatListData(fetchedData);
        for (let i of fetchedData) {
            if (i.isSelected) {
                setCurrentlyActive(i.topicId);
                // props.activateTopic(i.topicId);
                break;
            }
        }
        setFetchingInProgress(false);
    };
    useEffect(() => {
        props.activateTopic(currentlyActive);
    }, [currentlyActive])
    useEffect(() => {
        if (fetchingInProgress)
            try {
                fetchCategoriesData();
            }
            catch (e) {
                Toast.show(`${JSON.stringify(e)}`)
            }

        // console.log(`currentlyActive : ${currentlyActive}`);
    });
    const headerComponent = () => {
        if (fetchingInProgress)
            return (
                <View>
                    <Image
                        source={require('../../assets/animationGIFS/compass.gif')}
                        style={{ height: 60, width: 200, resizeMode: 'contain' }}
                    />
                </View>
            )

        return (
            <View></View>
        )
    };
    return (
        <SafeAreaView style={[styles.newsTypeSelectors]}>
            <FlatList
                style={[styles.newsTypeSelectorsFlatlist]}
                data={flatListData}
                horizontal={true}
                renderItem={({ item, index }) => {
                    return (<NewsSelector displayData={item} isActive={currentlyActive} activate={setCurrentlyActive} />)
                }}
                keyExtractor={(item, index) => index.toString()}
                ListHeaderComponent={headerComponent}
                showsHorizontalScrollIndicator={false}

            />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    newsTypeSelectors: {
        flex: 1,
        shadowColor: 'grey',
        shadowOpacity: 1,
        // backgroundColor:'white',
        borderBottomColor: 'grey',
        borderBottomWidth: 2,
    },
    newsTypeSelectorsFlatlist: {
        flex: 1
    }
});

export default NewsTypeSelection;