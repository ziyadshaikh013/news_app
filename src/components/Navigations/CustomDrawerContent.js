import React, {useState, useEffect} from 'react';
import {
    Text,
    View,
    Dimensions,
    StyleSheet,
    Image,
} from 'react-native';
import { DrawerContentScrollView, DrawerItemList, DrawerItem } from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/FontAwesome';
import { SafeAreaView } from 'react-native-safe-area-context';
import { ScrollView } from 'react-native-gesture-handler';


import { Auth } from '@aws-amplify/auth';
import Amplify from '@aws-amplify/core';
import awsConfig from '../../../src/aws-exports';


import SignOutPage from '../../screens/SignOutPage.js';
const CustomDrawerContent = (props) => {

    const [nameOfUser, setNameOfUser] = useState('');
    const [emailOfUser, setEmailOfUser] = useState('');
    const handleKnowMore = () => {
        console.log('know more')
    }

    const handleLogout = () => {
        console.log('logged out')
    }
    useEffect(()=>{
        if(!nameOfUser&&!emailOfUser)
        {
            Auth.currentUserInfo().
            then(
                resp=>
                {
                    setNameOfUser(resp.attributes.name);
                    setEmailOfUser(resp.attributes.email);
                }
            );
        }
    })
    return (
        <SafeAreaView>
            <ScrollView>
                <View style={styles.container}>
                    <View style={styles.backbtn}>
                        <Icon name="arrow-left" size={26} color="white" onPress={() => props.navigation.closeDrawer()} />
                    </View>
                    <View style={styles.userDet}>
                        {/* <Image
                            style={styles.profile}
                            source={{
                                uri:
                                    'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
                            }}
                        /> */}
                        <Icon name="user-circle" size={56} color="white" style={{marginRight:20, marginLeft:10}} />

                        
                        <View>
                            <Text
                                style={[styles.text, {
                                    fontWeight: 'bold',
                                    textTransform: 'capitalize',
                                    fontSize: 20,
                                }]}>
                                {nameOfUser}
            </Text>
                            <Text
                                style={[styles.text, {
                                    color: '#CAC6C6',
                                }]}>{emailOfUser}</Text>
                        </View>
                    </View>
                    <View style={styles.knowMore}>
                        <Text style={{ color: 'white', fontSize: 12, fontWeight: '600', }} onPress={handleKnowMore}>Know More &nbsp; <Icon name="angle-right" size={12} color="white" /></Text>
                    </View>
                </View>
                <DrawerContentScrollView {...props}>
                    <DrawerItemList {...props} />
                    {/* <DrawerItem
                        label="Log Out"
                        labelStyle={styles.logout}
                        icon={({ size }) => <Icon color='#C25C70' size={size} name='sign-out' />}
                        onPress={SignOutPage}
                    /> */}
                </DrawerContentScrollView>
            </ScrollView>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        height: Dimensions.get('window').height / 3,
        backgroundColor: '#32292E',
    },
    knowMore: {
        flex: 1,
        borderTopColor: '#707070',
        borderTopWidth: 0.5,
        justifyContent: 'center',
        alignItems: 'flex-end',
        paddingHorizontal: 10,
    },
    userDet: {
        flex: 4,
        flexDirection: 'row',
        alignItems: 'center',
    },
    backbtn: {
        paddingHorizontal: 10,
        paddingTop: 10,
    },
    profile: {
        height: 70,
        width: 70,
        marginHorizontal: 15,
        borderRadius: 50,
        borderWidth: 2,
        borderColor: 'white',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    text: {
        color: '#f9f9f9',
        fontFamily: 'open san',
    },
    logout: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#C25C70',
    }
});

export default CustomDrawerContent;
