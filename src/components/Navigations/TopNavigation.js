import React from 'react';
import { View, Image, StyleSheet, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const data = require('../../assets/translations/texts.json');

const TopNavigation = (props) => {
    return (
        <View style={styles.navigationBar}>
            <View style={styles.hamburgerIcon}>
                <TouchableOpacity onPress={()=>{props.setOverlay(true);props.navigation.navigate("Settings")}}>
                    <Icon name="gear" size={24} color='white' style={styles.iconStyle}></Icon>
                </TouchableOpacity>
            </View>
            <View style={styles.Btn}>
                <TouchableOpacity>
                    <Text style={[styles.BtnText, {color:'#d44e68'}]}>{data["News"][props.appLanguage]}</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.Btn}>
                <TouchableOpacity>
                    <Text style={styles.BtnText}>{data["Updates"][props.appLanguage]}</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    navigationBar: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#33292E',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    iconStyle: {
        alignContent: 'center',
        alignSelf: 'center',
    },
    Btn: {
        // backgroundColor:'#33292E',
        flex:0.4,
        alignContent: 'center',
        alignSelf: 'center',
    },
    BtnText: {
        color: 'white',
        fontSize: 20,
        alignContent: 'center',
        alignSelf: 'center',
    },
});

export default TopNavigation;