import React from 'react';
import { View, Image, StyleSheet, Text, TouchableOpacity } from 'react-native';
// import { SvgXml } from 'react-native-svg';
import HomeBtn from '../../assets/images/homeBtn.svg'
import SignOutPage from '../../screens/SignOutPage.js';
const data = require('../../assets/translations/texts.json');

const BottomNavigation = (props) => {
    return (
        <View style={styles.navigationBar}>
            <View style={[styles.Icon ]}>
                <TouchableOpacity>
                    <View style={[styles.homeIcon]}>
                        <Image
                            source={require('../../assets/images/homeIconWhite.png')}
                            style={[styles.iconStyle]}
                        />
                        <Text style={{ color: 'white', marginLeft: '10%' }}>{data["Home"][props.language]}</Text>
                    </View>
                </TouchableOpacity>

            </View>
            <View style={[styles.Icon]}>
                <TouchableOpacity>
                    <Image
                        source={require('../../assets/images/searchIconGrey.png')}
                        style={[styles.iconStyle]}
                    />
                </TouchableOpacity>
            </View>
            <View style={[styles.Icon]}>
                <TouchableOpacity>
                    <Image
                        source={require('../../assets/images/bookmarkIconGrey.png')}
                        style={[styles.iconStyle]}
                    />
                </TouchableOpacity>
            </View>
            <View style={[styles.Icon]}>
                <TouchableOpacity
                    onPress={()=>props.navigation.navigate('Sign Out Page')}
                >
                    <Image
                        source={require('../../assets/images/profileIconGrey.png')}
                        style={[styles.iconStyle]}
                    />
                </TouchableOpacity>
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    navigationBar: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#33292E',
    },
    Icon: {
        flex: 0.3,
        alignSelf: 'center',
    },
    homeIcon: {
        // flex:1,
        flexDirection: 'row',
        backgroundColor: 'black',
        borderRadius: 20,
        padding: 5,
        paddingRight: 15,
        paddingLeft: 15,
        resizeMode: 'contain',
        alignContent: 'center',
        alignSelf: 'center',
    },
    iconStyle: {
        width: 20,
        height: 20,
        alignSelf: 'center',
    },
});

export default BottomNavigation;